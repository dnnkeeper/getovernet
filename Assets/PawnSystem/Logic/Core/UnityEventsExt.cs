﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityFloatEvent : UnityEvent<float> { }

[System.Serializable]
public class UnityVector3Event : UnityEvent<Vector3> { }

[System.Serializable]
public class UnityGameObjectEvent : UnityEvent<GameObject> { }