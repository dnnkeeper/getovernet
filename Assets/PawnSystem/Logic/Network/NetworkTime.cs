﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkTime : NetworkBehaviour
{
    public float remoteDelayTime;
    public double networkTimeDelay;

    //public double timeOffset;

    public const short TimeMessageId = MsgType.Highest + 1;
    public class TimeMessage : MessageBase
    {
        //public double time;
        public double networkTime;
        public int timestamp;
    }

    public override void OnStartClient()
    {
        NetworkManager.singleton.client.RegisterHandler(TimeMessageId, OnMessageReceived);
    }

    public override void OnStartServer()
    {
        NetworkServer.RegisterHandler(TimeMessageId, OnMessageReceived);
    }

    void SendMessage()
    {
        TimeMessage msg = new TimeMessage();

        //msg.time = Time.time;

        msg.timestamp = NetworkTransport.GetNetworkTimestamp();
#if UNITY_5
        msg.networkTime = Network.time;
#endif
        if (isServer)
            connectionToClient.Send(TimeMessageId, msg);
        else
            connectionToServer.Send(TimeMessageId, msg);
    }

    private void OnMessageReceived(NetworkMessage netMsg)
    {
        var msg = netMsg.ReadMessage<TimeMessage>();


        //timeOffset = Time.time - msg.time;

        byte error;
        remoteDelayTime = NetworkTransport.GetRemoteDelayTimeMS(netMsg.conn.hostId, netMsg.conn.connectionId, msg.timestamp, out error);

#if UNITY_5
        networkTimeDelay = (Network.time - msg.networkTime) / 1000;
#endif
        Debug.Log(remoteDelayTime + " MS remoteDelayTime ");

#if UNITY_5
        Debug.Log(networkTimeDelay + " MS Network Time delay");
#endif

    }

    private void OnGUI()
    {
        GUILayout.Space(100);

        GUILayout.Label("remoteDelayTime = " + remoteDelayTime);
#if UNITY_5
        GUILayout.Label("networkTimeDelay = " + networkTimeDelay.ToString("F6") );
#else
        GUILayout.Label("networkTimeDelay = 0; USE UNITY 5 to measure");
#endif

        if (GUILayout.Button("Send SimpleMessage"))
        {
            SendMessage();
        }
    }
}
