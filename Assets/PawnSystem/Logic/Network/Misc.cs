﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;

namespace UPC.Utility
{

	public struct ShortRotator{

		public short Pitch, Yaw, Roll;

		public ShortRotator(short pitch, short yaw, short roll){
			Pitch = pitch;
			Yaw = yaw;
			Roll = roll;
		}

		public ShortRotator(Quaternion q){

			Vector3 euler = q.eulerAngles;

			Pitch = (short) (euler.x/360f*65535f);
			Yaw   = (short) (euler.y/360f*65535f);
			Roll  = (short) (euler.z/360f*65535f);
		}

		public Quaternion ToQuaternion(){
			return Quaternion.Euler ( (float)Pitch/65535f*360f, (float)Yaw/65535f*360f,  (float)Roll/65535f*360f );
		}
	}

	public struct ByteRotator{

		public sbyte Pitch, Yaw, Roll;

		public ByteRotator(Quaternion q){

			Vector3 euler = q.eulerAngles;

			Pitch = (sbyte) (euler.x/360f*255f);
			Yaw   = (sbyte) (euler.y/360f*255f);
			Roll  = (sbyte) (euler.z/360f*255f);
		}

		public Quaternion ToQuaternion(){
			return Quaternion.Euler ( (float)Pitch/255f*360f, (float)Yaw/255f*360f,  (float)Roll/255f*360f );
		}
	}
		
	public static class Misc {
		
		public static float AngleSignedWith(this Vector3 v1, Vector3 v2, Vector3 n)
		{
			return Mathf.Atan2(
				Vector3.Dot(n, Vector3.Cross(v1, v2)),
				Vector3.Dot(v1, v2));
		}

		public static void DrawAngle(Vector3 pos, Vector3 fwd, Vector3 up, float angle, float dt, Color c1, Color c2){
			float sign = Mathf.Sign (angle);
			float absAngle = Mathf.Abs (angle);
			for (int i = 0; i < absAngle; i++) {
				Debug.DrawLine (pos, pos + Quaternion.AngleAxis(sign*i, up) * fwd, Color.Lerp(c1, c2, i/absAngle), dt);
			}
			Debug.DrawLine (pos, pos + Quaternion.AngleAxis(sign*absAngle, up) * fwd * 2.0f, c2, dt);
			Debug.DrawLine (pos, pos + Quaternion.AngleAxis(0, up) * fwd * 2.0f, c1, dt);
		}

		public static uint compressQuat(this Quaternion q)
		{
			float w, x, y, z;
			w = q.w;
			x = q.x;
			y = q.y;
			z = q.z;

			//Debug.Log ( w+" "+x+" "+y+" "+z );

			int max_index = 0;
			float max_val = Mathf.Abs(w);
			if (Mathf.Abs(x) > max_val) { max_val = Mathf.Abs(x); max_index = 1;}
			if (Mathf.Abs(y) > max_val) { max_val = Mathf.Abs(y); max_index = 2;}
			if (Mathf.Abs(z) > max_val) { max_val = Mathf.Abs(z); max_index = 3;}

			int sign_max; 
			int a,b,c;
			a = b = c = 0;
			const int kSF = 722; // floor(Mathf.Sqrt(2)*511), the largest scale factor such that 2nd largest component fits in 9 bits

			switch(max_index) {
			case 0:
				sign_max = w > 0 ? kSF : -kSF;
				a =  (int)(sign_max * x); b =  (int)(sign_max * y); c =  (int)(sign_max * z);
				break;
			case 1:
				sign_max = x > 0 ? kSF : -kSF;
				a =  (int)(sign_max * w); b =  (int)(sign_max * y); c =  (int)(sign_max * z);
				break;
			case 2:
				sign_max = y > 0 ? kSF : -kSF;
				a =  (int)(sign_max * x); b =  (int)(sign_max * w); c =  (int)(sign_max * z);
				break;
			case 3:
				sign_max = z > 0 ? kSF : -kSF;
				a = (int)(sign_max * x); b = (int)(sign_max * y); c = (int)(sign_max * w);
				break;
			}

			const int k10bitmask = 0x3FF;

			uint compressed_quat = (uint)( (max_index << 30) | ((a & k10bitmask) << 20) | ((b & k10bitmask) << 10) | (c & k10bitmask) );

			//Debug.Log ("compressed "+w+" "+x+" "+y+" "+z+" = "+compressed_quat );
			return compressed_quat;
		}

		public static Quaternion uncompressQuaterion(this uint cq)
		{
			const int k10bitmask = 0x3FF;
			const float kSF = 1.0f/722f;
			int max_ind = (int) (cq >> 30);
			int a = (int)( (cq >> 20) & k10bitmask);
			int b = (int)( (cq >> 10) & k10bitmask);
			int c = (int)( cq & k10bitmask);

			// turn back into a 10bit signed value
			if (a >= 512) { a -= 1024; }
			if (b >= 512) { b -= 1024; }
			if (c >= 512) { c -= 1024; }

			float w,x,y,z;
			w = x = y = z = 0;
			switch (max_ind) {
			case 0:
				x = a*kSF; y = b*kSF; z = c*kSF;
				w = Mathf.Sqrt(1.0f - x * x - y * y - z * z);
				break;
			case 1:
				w = a*kSF; y = b*kSF; z = c*kSF;
				x = Mathf.Sqrt(1.0f - w * w - y * y - z * z);
				break;
			case 2:
				x = a*kSF; w = b*kSF; z = c*kSF;
				y = Mathf.Sqrt(1.0f - x * x - w * w - z * z);
				break;
			case 3:
				x = a*kSF; y = b*kSF; w = c*kSF;
				z = Mathf.Sqrt(1.0f - x * x - y * y - w * w);
				break;
			}
			//Debug.Log ("decompressed = "+w+" "+x+" "+y+" "+z );
			return new Quaternion(x,y,z,w);
		}

	}
}
