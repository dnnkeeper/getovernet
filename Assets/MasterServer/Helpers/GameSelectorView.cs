﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace MasterServer {
	public class GameSelectorView : MonoBehaviour {
		[SerializeField] private Image _background;
		[SerializeField] private Gradient _gradient;
		[SerializeField] private Text _version;
		[SerializeField] private Text _message;
		[SerializeField] private float _speed;
		[SerializeField] private InputField _nickname;
		[SerializeField] private Button _cancel;
		[SerializeField] private Button _connect;
		[SerializeField] private Button _pvp;
		[SerializeField] private Button _race;
		[SerializeField] private GameObject _menu;
		[SerializeField] private GameObject _wait;
		[SerializeField] private GameObject _username;
		public string Version { set => _version.text = value; }
		public string Nickname { set => _nickname.text = value; }
		private float _color;
		
		public void Unlisten() {
			_connect.onClick.RemoveAllListeners();
			_race.onClick.RemoveAllListeners();
			_pvp.onClick.RemoveAllListeners();
			_cancel.onClick.RemoveAllListeners();
			_nickname.onValueChanged.RemoveAllListeners();
		}

		public void Refresh() {
			_background.color = _gradient.Evaluate(_color);
			_color = (_color + Time.deltaTime * _speed) % 1f;
		}

		public void Wait(string message, Action onCancel) {
			Unlisten();
			_cancel.onClick.AddListener(onCancel.Invoke);
			_message.text = message;
			
			_wait.SetActive(true);
			_username.SetActive(false);
			_menu.SetActive(false);
		}

		public void Username(Action onConnect, Action<string> onNickname) {
			Unlisten();
			_connect.onClick.AddListener(onConnect.Invoke);
			_nickname.onValueChanged.AddListener(onNickname.Invoke);
			
			_wait.SetActive(false);
			_username.SetActive(true);
			_menu.SetActive(false);
		}

		public void Menu(Action onRace, Action onPvp) {
			Unlisten();
			_race.onClick.AddListener(onRace.Invoke);
			_pvp.onClick.AddListener(onPvp.Invoke);
			
			_wait.SetActive(false);
			_username.SetActive(false);
			_menu.SetActive(true);
		}
	}
}
