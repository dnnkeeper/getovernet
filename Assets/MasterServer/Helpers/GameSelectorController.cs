﻿using System;
using MasterServer.StateMachines;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace MasterServer.Helpers {
	public class GameSelectorController : IInitializable, IDisposable, ITickable {
		private const string NicknameKey = "Client.Settings.Nickname";
		private readonly GameSelectorView _view;
		private readonly ClientSettings _settings;
		private readonly ConnectToMaster _connection;
		private string _nickname;

		public GameSelectorController(
			GameSelectorView view,
			ClientSettings settings,
			ConnectToMaster connection
		) {
			_view       = view;
			_settings   = settings;
			_connection = connection;
#if UNITY_EDITOR
			_nickname = $"Guest{Random.Range(0, 100000)}";
#else
			_nickname = PlayerPrefs.GetString(NicknameKey, $"Guest{Random.Range(0, 100000)}");
#endif
		}

		public void Initialize() {
			_view.Username(OnConnect, OnNickname);
			_view.Nickname = _nickname;
			_view.Version  = _settings.Version;
		}

		private void OnNickname(string value) { _nickname = value; }

		public void Dispose() { _view.Unlisten(); }

		private void OnConnect() {
			_view.Wait("Wait for authorization...", OnCancel);
			_connection.Connect(_nickname, OnConnected);
			PlayerPrefs.SetString(NicknameKey, _nickname);
			PlayerPrefs.Save();
		}


		private void OnCancel() {
			_view.Username(OnConnect, OnNickname);
			_connection.Cancel();
		}

		private void OnConnected() { _view.Menu(OnRace, OnPvp); }

		private void OnRace() {
			_view.Wait("Wait for Race game...", OnCancel);
			_connection.RequestFor("race");
		}

		private void OnPvp() {
			_view.Wait("Wait for PVP game...", OnCancel);
			_connection.RequestFor("pvp");
		}

		public void Tick() { _view.Refresh(); }
	}
}
