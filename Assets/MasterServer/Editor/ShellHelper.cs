﻿using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;

namespace MasterServer {
	public class ShellHelper {
		public class ShellRequest {
			public event System.Action<int, string> OnLog;
			public event System.Action OnError;
			public event System.Action OnDone;

			public void Log(int type, string log) {
				OnLog?.Invoke(type, log);
				if (type == 1) UnityEngine.Debug.LogError(log);
			}

			public void NotifyDone() => OnDone?.Invoke();

			public void Error() => OnError?.Invoke();
		}


		private static string ShellApp {
			get {
#if UNITY_EDITOR_WIN
			string app = "cmd.exe";
			#elif UNITY_EDITOR_OSX
				var app = "bash";
#endif
				return app;
			}
		}


		private static readonly List<System.Action> Queue = new List<System.Action>();


		static ShellHelper() {
			EditorApplication.update += OnUpdate;
		}

		private static void OnUpdate() {
			for (var i = 0; i < Queue.Count; i++) {
				try {
					var action = Queue[i];
					action?.Invoke();
				}
				catch (System.Exception e) { UnityEngine.Debug.LogException(e); }
			}
			Queue.Clear();
		}


		public static ShellRequest ProcessCommand(string cmd, string workDirectory, List<string> environmentVars = null) {
			var req = new ShellRequest();
			System.Threading.ThreadPool.QueueUserWorkItem(delegate(object state) {
				Process p = null;
				try {
					var start = new ProcessStartInfo(ShellApp);

#if UNITY_EDITOR_OSX
					var splitChar = ":";
					start.Arguments = "-c";
#elif UNITY_EDITOR_WIN
				string splitChar = ";";
				start.Arguments = "/c";
				#endif

					if (environmentVars != null) {
						foreach (var var in environmentVars) { start.EnvironmentVariables["PATH"] += (splitChar + var); }
					}

					start.Arguments        += (" \"" + cmd + " \"");
					start.CreateNoWindow   =  true;
					start.ErrorDialog      =  true;
					start.UseShellExecute  =  false;
					start.WorkingDirectory =  workDirectory;

					if (start.UseShellExecute) {
						start.RedirectStandardOutput = false;
						start.RedirectStandardError  = false;
						start.RedirectStandardInput  = false;
					}
					else {
						start.RedirectStandardOutput = true;
						start.RedirectStandardError  = true;
						start.RedirectStandardInput  = true;
						start.StandardOutputEncoding = System.Text.UTF8Encoding.UTF8;
						start.StandardErrorEncoding  = System.Text.UTF8Encoding.UTF8;
					}

					p = Process.Start(start);
					p.ErrorDataReceived += delegate(object sender, DataReceivedEventArgs e) {
						UnityEngine.Debug.LogError(e.Data);
					};
					p.OutputDataReceived += delegate(object sender, DataReceivedEventArgs e) {
						UnityEngine.Debug.LogError(e.Data);
					};
					p.Exited += delegate(object sender, System.EventArgs e) { UnityEngine.Debug.LogError(e.ToString()); };

					var hasError = false;
					do {
						var line = p.StandardOutput.ReadLine();
						if (line == null) { break; }
						line = line.Replace("\\", "/");

						Queue.Add(delegate() { req.Log(0, line); });
					} while (true);

					while (true) {
						var error = p.StandardError.ReadLine();
						if (string.IsNullOrEmpty(error)) break;
						hasError = true;
						Queue.Add(() => req.Log(1, error));
					}
					p.Close();
					if (hasError) { Queue.Add(() => req.Error()); }
					else { Queue.Add(() => req.NotifyDone()); }
				}
				catch (System.Exception e) {
					UnityEngine.Debug.LogException(e);
					p?.Close();
				}
			});
			return req;
		}


		private readonly List<string> _enviroumentVars = new List<string>();

		public void AddEnvironmentVars(params string[] vars) {
			for (var i = 0; i < vars.Length; i++) {
				if (vars[i] == null) { continue; }
				if (string.IsNullOrEmpty(vars[i].Trim())) { continue; }
				_enviroumentVars.Add(vars[i]);
			}
		}

		public ShellRequest ProcessCmd(string cmd, string workDir) {
			return ShellHelper.ProcessCommand(cmd, workDir, _enviroumentVars);
		}
	}
}
