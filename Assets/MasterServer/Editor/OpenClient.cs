﻿using System;
using System.Diagnostics;
using System.Threading;
using UnityEditor;

namespace MasterServer.Editor {
	public class OpenClient {
		private const string FileName =
			"/Users/weslompo/Unity/GetOverNet/Builds/MsfInstance.app/Contents/MacOS/MsfInstance";

		[MenuItem("Tools/Open client")]
		private static void Client() { Open("-client"); }

		[MenuItem("Tools/Open server")]
		private static void Server() { Open("-msfStartMaster -msfStartSpawner"); }

		private static void Open(string args) {
			try {
				new Thread(() => {
					try {
						using (var myProcess = new Process() {
							StartInfo = {
								WindowStyle = ProcessWindowStyle.Normal,
								CreateNoWindow = true,
								UseShellExecute = false,
								FileName = FileName,
								Arguments = args
							},
							EnableRaisingEvents = true
						}) {
							myProcess.Start();
							myProcess.WaitForExit();
							var code = myProcess.ExitCode;
							UnityEngine.Debug.Log("[OpenClient] Closed with code: " + code);
						}
					}
					catch (Exception e) { UnityEngine.Debug.Log("[OpenClient] Process Error:" + e); }
				}).Start();
			}
			catch (Exception e) { UnityEngine.Debug.Log("[OpenClient] Thread Error:" + e); }
		}

		[MenuItem("Tools/Build/Linux64")]
		public static void BuildLinux() {
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneLinux64);
		}

		[MenuItem("Tools/Build/Windows64")]
		public static void BuildWindows() {
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
		}

		[MenuItem("Tools/Build/OSX")]
		public static void BuildMac() {
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneOSX);
		}
	}
}
