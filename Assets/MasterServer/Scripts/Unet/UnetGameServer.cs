﻿using Barebones.Logging;
using Barebones.MasterServer;
using MasterServer.Packets;

namespace MasterServer.Unet {
	public class UnetGameServer {
		private readonly BmLogger _logger = Msf.Create.Logger(typeof(UnetGameServer).Name);
		private readonly UnetNetworkManager _unetNetworkManager;

		public UnetGameServer(
			LogLevelSettings logLevelSettings,
			GameServer gameServer,
			UnetNetworkManager unetNetworkManager
		) {
			_logger.LogLevel = logLevelSettings.Current;
			_unetNetworkManager = unetNetworkManager;
			gameServer.Registered += Registered;
			gameServer.GameStarted += GameStarted;
			gameServer.GameEnded += GameEnded;
			_unetNetworkManager.onlineScene = "MultiplayerPVP";
		}

		private void GameStarted() { _logger.Info("GameStarted"); }

		private void GameEnded() {
			_logger.Info("GameEnded");
			_unetNetworkManager.HostDisconnect();
		}

		private void Registered(GameServerMatchDetailsPacket details) {
			_logger.Info($"Registered {details.GamePort}");
			_unetNetworkManager.HostConnect(details.GamePort);
		}
	}
}
