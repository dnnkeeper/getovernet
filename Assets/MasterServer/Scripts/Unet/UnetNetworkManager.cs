﻿using System;
using Barebones.Logging;
using Barebones.MasterServer;
using GetOverNet.Interfaces;
using UnityEngine;
using UnityEngine.Networking;

namespace MasterServer.Unet {
	public class UnetNetworkManager : NetworkManager, IServerPlayerEvents {
		
		public event Action<NetworkConnection, short> OnServerAddPlayerEvent;
		public event Action<NetworkConnection, PlayerController> OnServerRemovePlayerEvent;

		private bool _isServer = false;

		public override void OnClientConnect(NetworkConnection conn) {
			if (_isServer)
				return;
			ClientScene.AddPlayer(conn, 0);
		}

		public override void OnServerConnect(NetworkConnection conn) => _isServer = true;

		public override void OnClientDisconnect(NetworkConnection conn)
			=> Debug.Log("NetworkManager OnClientDisconnect");

		public override void OnClientError(NetworkConnection conn, int errorCode)
			=> Debug.Log($"NetworkManager OnClientError {errorCode}");

		public void HostConnect(int port) {
			networkPort = port;
			StartServer();
		}

		public void HostDisconnect() => StopServer();

		public void ClientConnect(string address, int port) {
			networkPort    = port;
			networkAddress = address;
			StartClient();
		}

		public void ClientDisconnect() => StopClient();

		public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
			=> OnServerAddPlayerEvent?.Invoke(conn, playerControllerId);

		public override void OnServerAddPlayer(NetworkConnection connection, short playerId, NetworkReader reader)
			=> OnServerAddPlayerEvent?.Invoke(connection, playerId);

		public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
			=> OnServerRemovePlayerEvent?.Invoke(conn, player);
	}
}
