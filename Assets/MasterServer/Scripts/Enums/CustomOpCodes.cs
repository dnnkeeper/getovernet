namespace MasterServer.Enums {
	public enum CustomOpCodes : short {
		RequestStartGame = 1,
		GameServerMatchDetails,
		GameServerMatchCompletion
	}
}
