﻿namespace MasterServer.Enums {
	public enum ClientState {
		Initial,
		ConnectingToMaster,
		ConnectedToMaster,
		FailedToConnectToMaster,
		LoggingIn,
		LoggedIn,
		FailedToLogIn,
		BetweenGames,
		RequestedGame,
		AssignedGame,
		FailedToGetGame,
		ConnectingToGameServer,
		ConnectedToGameServer,
		FailedToConnectToGameServer,
		GameStarted,
		GameEnded,
		DisconnectedFromGameServer,
		DisconnectedFromMaster,
		Stop
	};
}
