namespace MasterServer.Enums {
	public enum GameServerState {
		Initial,
		ConnectingToMaster,
		ConnectedToMaster,
		FailedToConnectToMaster,
		Registering,
		Registered,
		FailedToRegister,
		WaitingForClients,
		GameStarted,
		GameEnded,
		DisconnectedFromMaster,
		Stop
	}
}
