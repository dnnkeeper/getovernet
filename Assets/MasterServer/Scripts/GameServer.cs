using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Barebones.Logging;
using Barebones.MasterServer;
using Barebones.Networking;
using MasterServer.Enums;
using MasterServer.Extensions;
using MasterServer.Packets;
using UnityEngine;

namespace MasterServer {
	public class GameServer {
		private readonly InstanceSettings _settings;
		private readonly BuiltinSignalsProvider _builtinSignalsProvider;
		private readonly BmLogger _logger = Msf.Create.Logger(typeof(GameServer).Name);
		private readonly int _gamePort;
		private readonly List<IPeer> _clients;

		private SpawnTaskController _currentSpawnTaskController;
		private int _userCount;
		private IServerSocket _gameServerSocket;

		private GameServerState GameServerState { get; set; }

		public string Status { get; set; }

//		private ColorBlock _highlightedColorBlock;


		public event Action<GameServerMatchDetailsPacket> Registered;
		public event Action GameStarted;
		public event Action GameEnded;


		public GameServer(
			LogLevelSettings logLevelSettings,
			InstanceSettings settings,
			BuiltinSignalsProvider builtinSignalsProvider
		) {
			_settings               = settings;
			_builtinSignalsProvider = builtinSignalsProvider;
			_logger.LogLevel        = logLevelSettings.Current;
//			
//		}
//		
//		private void ShowGameServerState() {
////			if ((int) GameServerState < Buttons.Length &&
////			    Buttons[(int) GameServerState] != null) {
////				Buttons[(int) GameServerState].Select();
////				Buttons[(int) GameServerState].colors = _highlightedColorBlock;
////			}
//		}
//
//		public void Awake() {
////			_logger.LogLevel = LogLevel;
//
////			ButtonUi.gameObject.SetActive(true);

//			_highlightedColorBlock = ColorBlock.defaultColorBlock;
//			_highlightedColorBlock.highlightedColor = new Color(135 / 255f, 206 / 255f, 250 / 255f);

			if (Msf.Args.IsProvided("-gamePort")) { _gamePort = Msf.Args.ExtractValueInt("-gamePort"); }

			DumpCommandLineArgs();

			_clients = new List<IPeer>();

			SetupGameServerSocket();

			GoToState_Initial();
		}

		private void DumpCommandLineArgs() {
			var args = string.Empty;

			foreach (var arg in Environment.GetCommandLineArgs()) { args += arg + " "; }
			args = args.TrimEnd();
			Debug.Log(args);
		}


		private IEnumerator _backgroundEnumerator;

		private void StopBackgroundEnumerator() {
			if (_backgroundEnumerator != null) {
				_builtinSignalsProvider.StopCoroutine(_backgroundEnumerator);
				_backgroundEnumerator = null;
			}
		}

		private void GoToState_Initial() {
			GameServerState = GameServerState.Initial;
			Status          = string.Empty;

			_builtinSignalsProvider.StartCoroutine(
				CoroutineUtils.StartWaiting(_settings.WaitingToConnectToMaster,
					GoToState_ConnectingToMaster,
					1f,
					time => { Status = $"Waiting {time}s"; },
					false)
			);
		}

		private void GoToState_ConnectingToMaster() {
			GameServerState = GameServerState.ConnectingToMaster;
			Status          = string.Empty;

			// connect to the master server
			Msf.Connection.Connected    += Connected;
			Msf.Connection.Disconnected += Disconnected;

			_logger.Info($"Connecting to master on {Msf.Args.MasterIp}:{Msf.Args.MasterPort}");

			_backgroundEnumerator = CoroutineUtils.StartWaiting(_settings.ConnectToMasterTimeout,
				() => { GoToState_FailedToConnectToMaster("Timed out"); },
				1f,
				time => { Status = $"Trying to connect {time}s"; });
			_builtinSignalsProvider.StartCoroutine(_backgroundEnumerator);

			Msf.Connection.Connect(Msf.Args.MasterIp, Msf.Args.MasterPort);
		}

		private void Disconnected() { GoToState_DisconnectedFromMaster(); }

		private void Connected() { GoToState_ConnectedToMaster(); }

		private void GoToState_ConnectedToMaster() {
			StopBackgroundEnumerator();

			GameServerState = GameServerState.ConnectedToMaster;
			Status          = string.Empty;

			GoToState_Registering();
		}


		private void GoToState_FailedToConnectToMaster(string message) {
			GameServerState = GameServerState.FailedToConnectToMaster;
			Status          = message;

			Msf.Connection.Connected    -= Connected;
			Msf.Connection.Disconnected -= Disconnected;

			_builtinSignalsProvider.StartCoroutine(CoroutineUtils.StartWaiting(_settings.QuitDelay,
				() => { GoToState_Stop(); }));
		}

		private void GoToState_Stop() {
			GameServerState = GameServerState.Stop;
			Status          = string.Empty;

			Application.Quit();
		}

		private void GoToState_Registering() {
			GameServerState = GameServerState.Registering;
			Status          = string.Empty;

			var spawnId   = Msf.Args.SpawnId == -1 ? 0 : Msf.Args.SpawnId;
			var spawnCode = Msf.Args.SpawnCode == null ? "5acc3d" : Msf.Args.SpawnCode;
			_logger.Info($"spawnerId: {spawnId}  spawnerCode: {spawnCode}");

			Msf.Server.Spawners.RegisterSpawnedProcess(spawnId, spawnCode, HandleRegistered);
		}

		private void HandleRegistered(SpawnTaskController taskController, string error) {
			_currentSpawnTaskController = taskController;

			if (_currentSpawnTaskController == null) { throw new Exception("HandleRegistered null taskController"); }

			_userCount = 1;

			if (taskController.Properties == null) { throw new Exception("HandleRegistered null Properties"); }

			if (_currentSpawnTaskController.Properties.ContainsKey("UserCount")) {
				_userCount = int.Parse(_currentSpawnTaskController.Properties["UserCount"]);
				_logger.Info($"Match scheduled with {_userCount} users.");
			}

			// log the property dictionary
			//foreach (KeyValuePair property in taskController.Properties)
			//{
			//    Logger.Info(string.Format("{0} = {1}", property.Key, property.Value));
			//}

			_logger.Info("Listening for game clients on port: " + Msf.Args.AssignedPort);
			Debug.Log("LISTEN");
			_gameServerSocket.Listen(Msf.Args.AssignedPort);

			GoToState_Registered();
		}

		private void GoToState_Registered() {
			GameServerState = GameServerState.Registered;
			Status          = string.Empty;

			SendGameServerMatchDetails();

			GoToState_WaitingForClients();
		}

		private void SetupGameServerSocket() {
			_gameServerSocket              =  new ServerSocketUnet(); // todo Inject
			_gameServerSocket.Connected    += ClientConnected;
			_gameServerSocket.Disconnected += ClientDisconnected;
		}

		private void ClientConnected(IPeer client) {
			Status = $"ClientConnected: Id {client.Id}";
			_logger.Info(Status);

			_clients.Add(client);
			if (_clients.Count == _userCount) {
				_builtinSignalsProvider.StartCoroutine(CoroutineUtils.StartWaiting(3f, () => { GoToState_GameStarted(); }));
			}
		}

		private void ClientDisconnected(IPeer client) { _clients.Remove(client); }

		private void GoToState_WaitingForClients() {
			GameServerState = GameServerState.WaitingForClients;
			Status          = string.Empty;

			_backgroundEnumerator = CoroutineUtils.StartWaiting(_settings.WaitingForClients,
				() => { GoToState_GameEnded(false); },
				1f,
				time => { Status = $"Waiting for clients {time}s"; });
			_builtinSignalsProvider.StartCoroutine(_backgroundEnumerator);
		}

		private void SendGameServerMatchDetails() {
			var details = new GameServerMatchDetailsPacket() {
				SpawnId      = Msf.Args.SpawnId,
				MachineId    = Msf.Args.MachineIp,
				AssignedPort = Msf.Args.AssignedPort,
				SpawnCode    = Msf.Args.SpawnCode,
				GamePort     = _gamePort
			};

			_logger.Info(
				$"SpawnId: {details.SpawnId}  MachineId: {details.MachineId}  AssignedPort: {details.AssignedPort}  SpawnCode: {details.SpawnCode}  GameSecondaryPort: {details.GamePort}");
			_logger.Info("send gameservermatchdetails");

			Msf.Connection.SendMessage((short) CustomOpCodes.GameServerMatchDetails,
				details,
				(status, response) => {
					if (status != ResponseStatus.Success) { _logger.Debug("Failed to get response"); }
				});
			Registered?.Invoke(details);
		}

		private void GoToState_GameStarted() {
			StopBackgroundEnumerator();

			GameServerState = GameServerState.GameStarted;
			Status          = string.Empty;

			BroadcastMessage(MessageHelper.Create(0, "Start Game"));

			_builtinSignalsProvider.StartCoroutine(CoroutineUtils.StartWaiting(_settings.GameDuration,
				() => GoToState_GameEnded(),
				1f,
				time => {
					Status = $"Game Count Down {time}";
					BroadcastMessage(MessageHelper.Create(0, Status));
				},
				false));
			GameStarted?.Invoke();
		}

		private void GoToState_GameEnded(bool success = true) {
			GameServerState = GameServerState.GameEnded;
			Status          = string.Empty;

			BroadcastMessage(MessageHelper.Create(0, "End Game"));

			var msg = Msf.Create.Message((short) CustomOpCodes.GameServerMatchCompletion,
				new GameServerMatchCompletionPacket {
					SpawnId = Msf.Args.SpawnId,
					Success = success
				});
			Msf.Connection.Peer.SendMessage(msg);

			GoToState_DisconnectedFromMaster();
			GameEnded?.Invoke();
		}

		private void GoToState_DisconnectedFromMaster() {
			GameServerState = GameServerState.DisconnectedFromMaster;
			Status          = string.Empty;

			// could close gameserversocket, connection to master
			_builtinSignalsProvider.StartCoroutine(CoroutineUtils.StartWaiting(_settings.QuitDelay, GoToState_Stop));
		}

		private void BroadcastMessage(IMessage msg) {
			_logger.Info(Encoding.UTF8.GetString(msg.Data));
			foreach (var client in _clients) { client.SendMessage(msg, DeliveryMethod.Reliable); }
		}

//		private void OnGUI() {
//			GUIStyle style = new GUIStyle();
//			Rect rect = new Rect(2, 2, Screen.width, style.fontSize);
//			style.fontSize = 14;
//			style.alignment = TextAnchor.UpperLeft;
////			style.normal.textColor = Color.white;
//			style.normal.textColor = Color.black;
//			string text = string.Format("[GameServer | {0}] {1}",
//				GameServerState,
//				Status);
//			GUI.Label(rect, text, style);
//		}
	}
}
