﻿using UnityEngine;

namespace MasterServer {
	[CreateAssetMenu(menuName = "MasterServer/InstanceSettings", fileName = "InstanceSettings")]
	public class InstanceSettings : ScriptableObject {
		public int GameDuration = 120;
		public float QuitDelay = 10;

		public float WaitingToConnectToMaster = 0f;
		public float WaitingForClients = 10f;
		public float ConnectToMasterTimeout = 10f;
	}
}
