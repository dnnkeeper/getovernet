using System;

namespace MasterServer {
	[Serializable]
	public class MatchmakerSettings {
		public int PlayersPerMatch = 2;
		public int MaxPlayersPerMatch = 4;
		public float PollFrequency = 2f;
		public float MatchFrequency = .5f;
		public float WaitForFull = 5f;
	}
}
