﻿using UnityEngine;

namespace MasterServer {
	[CreateAssetMenu(menuName = "MasterServer/ClientSettings", fileName = "ClientSettings")]
	public class ClientSettings : ScriptableObject {
		public string Version = "0.0.1";
		[SerializeField]private string _serverIp;
		[SerializeField]private bool _localhost;
		public string ServerIp => _localhost ? "127.0.0.1" : _serverIp;
		public int ServerPort = 5000;
		
		public float ConnectToMasterTimeout = 10f;
		public float ConnectToGameServerTimeout = 60f;
		public float LogInTimeout = 5f;

		public float WaitingToConnectToMaster = 1f;
		public float RequestedGameTimeout = 300f;
		public float WaitingBetweenGames = 2f;
	}
}
