﻿using UnityEngine;

namespace MasterServer {
	[CreateAssetMenu(menuName = "MasterServer/ServerSettings", fileName = "ServerSettings")]
	public class ServerSettings : ScriptableObject {
		[SerializeField] private bool _localhost;

		[Header("Server")] 
		[SerializeField] private string _ip = "127.0.0.1";
		public string Ip => _localhost ? "127.0.0.1" : _ip;
		public int Port = 5000;
		[Header("Spawner")] public int StartingPort = 2600;
		public int EndingPort = 2700;

		[Header("Spawner Settings")] 
		[SerializeField] private string _defaultMachineIp = "127.0.0.1";
		public string DefaultMachineIp => _localhost ? "127.0.0.1" : _defaultMachineIp;

		[SerializeField] private string _runtimeLogs;
		[SerializeField] private string _editorLogs;
		public string LogsPath => _localhost ? _editorLogs : _runtimeLogs;
		
		public string RuntimeExecutablePath;
		public string EditorExecutablePath;
		public string ExecutablePath => _localhost ? EditorExecutablePath : RuntimeExecutablePath;
		public bool DefaultSpawnInBatchmode = false;
		public int MaxSpawnedProcesses = 5;
	}
}
