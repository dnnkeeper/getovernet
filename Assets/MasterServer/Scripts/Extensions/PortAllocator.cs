using System.Collections.Generic;

namespace MasterServer.Extensions {
	public class PortAllocator {
		private readonly int _startingPort;
		private readonly int _endingPort;
		private readonly Queue<int> _freePorts = new Queue<int>();

		private int _lastPortTaken = -1;

		public PortAllocator(int startingPort, int endingPort) {
			_startingPort = startingPort;
			_endingPort = endingPort;
		}

		public int GetAvailablePort() {
			// Return a port from a list of available ports
			if (_freePorts.Count > 0) { return _freePorts.Dequeue(); }

			if (_lastPortTaken < 0) {
				_lastPortTaken = _startingPort;
				return _lastPortTaken;
			}

			if (_lastPortTaken < _endingPort) {
				_lastPortTaken += 1;
				return _lastPortTaken;
			}

			return -1;
		}

		public void ReleasePort(int port) { _freePorts.Enqueue(port); }
	}
}
