﻿using UnityEngine;
using Zenject;

namespace MasterServer {
	public class BuiltinSignalsProvider : MonoBehaviour {
		[Inject] private OnApplicationQuit _onApplicationQuit;
		private void OnApplicationQuit() => _onApplicationQuit.Fire();

		private void Awake() { DontDestroyOnLoad(gameObject); }
	}
}
