﻿using System;
using Barebones;
using Barebones.MasterServer;
using Barebones.Networking;
using MasterServer.Helpers;
using MasterServer.Spawners;
using MasterServer.StateMachines;
using MasterServer.Unet;
using UnityEngine;
using Zenject;

namespace MasterServer.Installers {
	public class MainInstaller : MonoInstaller {
		[SerializeField] private ApplicationType _kind;
		public override void InstallBindings() {
			Container.DeclareSignal<OnApplicationQuit>();
			Container.DeclareSignal<SetGameState>();
			
			var guiConsole = Container.Resolve<GUIConsole>();
#if UNITY_EDITOR
			var kind = _kind;
#else
			var kind = GetApplicationType();
#endif
			D.Log("[MainInstaller]","InstallBindings",kind);
			switch (kind) {
				case ApplicationType.Client:
					Container.BindInterfacesAndSelfTo<IClientSocket>().FromInstance(Msf.Connection).AsSingle();
					Container.BindInterfacesAndSelfTo<MsfClient>().FromInstance(Msf.Client).AsSingle();
					Container.BindInterfacesAndSelfTo<ConnectToMaster>().AsSingle();
					Container.BindInterfacesAndSelfTo<GameSelectorController>().AsSingle().NonLazy();
					guiConsole.Show = true;
					break;
				case ApplicationType.Server:
					Container.BindInterfacesAndSelfTo<IClientSocket>().FromInstance(Msf.Connection).AsSingle();
					Container.BindInterfacesAndSelfTo<Spawner>().AsSingle();
					Container.BindInterfacesAndSelfTo<SpawnerManager>().AsSingle();
					Container.BindInterfacesAndSelfTo<Matchmaker>().AsSingle();
					Container.BindInterfacesAndSelfTo<MasterServerRunner>().AsSingle().NonLazy();
					guiConsole.Show = true;
					break;
				case ApplicationType.Instance:
					Container.BindInterfacesAndSelfTo<GameServer>().AsSingle();
					Container.BindInterfacesAndSelfTo<UnetGameServer>().AsSingle().NonLazy();
					guiConsole.Show = false;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private static ApplicationType GetApplicationType() {
			#if UNITY_ANDROID
			return ApplicationType.Client;
			#endif
			if (Msf.Args.StartMaster)
				return ApplicationType.Server;
			if (Msf.Args.IsProvided("-client"))
				return ApplicationType.Client;
			if (Msf.Args.SpawnId != -1)
				return ApplicationType.Instance;
			throw new Exception("Unknown application type!");
		}

		private enum ApplicationType {
			Client,
			Server,
			Instance
		}
	}
}
