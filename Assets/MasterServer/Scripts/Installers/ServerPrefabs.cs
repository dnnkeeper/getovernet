﻿using Barebones;
using Barebones.Logging;
using Barebones.MasterServer;
using MasterServer.Unet;
using UnityEngine;
using Zenject;

namespace MasterServer.Installers {
	[CreateAssetMenu(menuName = "ServerPrefabs", fileName = "ServerPrefabs")]
	public class ServerPrefabs : ScriptableObjectInstaller {
		[SerializeField] private LogLevel _logLevel;

		[SerializeField] private ServerSettings _serverSettings;
		[SerializeField] private InstanceSettings _instanceSettings;
		[SerializeField] private ClientSettings _clientSettings;
		[SerializeField] private MatchmakerSettings _matchmakerSettings;
		[SerializeField] private BuiltinSignalsProvider _coroutine;

		[SerializeField] private GUIConsole _guiConsole;
		[SerializeField] private MasterServerBehaviour _masterServer;
		[SerializeField] private UnetNetworkManager _networkManager;
		[SerializeField] private GameSelectorView _selectorView;

		public override void InstallBindings() {
			Container.BindInterfacesAndSelfTo<BuiltinSignalsProvider>().FromNewComponentOnNewPrefab(_coroutine).AsSingle();
			Container.BindInterfacesAndSelfTo<UnetNetworkManager>().FromComponentInNewPrefab(_networkManager).AsSingle();
			Container.BindInterfacesAndSelfTo<GUIConsole>().FromComponentInNewPrefab(_guiConsole).AsSingle();
			Container.BindInterfacesAndSelfTo<MasterServerBehaviour>().FromComponentInNewPrefab(_masterServer).AsSingle();
			Container.BindInterfacesAndSelfTo<GameSelectorView>().FromComponentInNewPrefab(_selectorView).AsSingle();

			Container.BindInstance(new LogLevelSettings {Current = _logLevel});
			Container.BindInstance(_serverSettings);
			Container.BindInstance(_instanceSettings);
			Container.BindInstance(_clientSettings);
			Container.BindInstance(_matchmakerSettings);
		}
	}
}
