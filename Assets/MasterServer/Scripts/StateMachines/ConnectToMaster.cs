﻿using System;
using Barebones.MasterServer;
using Barebones.Networking;
using MasterServer.Enums;
using MasterServer.Extensions;
using MasterServer.Packets;
using MasterServer.Unet;
using Random = UnityEngine.Random;

namespace MasterServer.StateMachines {
	public class ConnectToMaster {
		private readonly BuiltinSignalsProvider _builtinSignalsProvider;
		private ClientSocketUnet _gameServerSocket;
		private readonly ClientSettings _settings;
		private readonly IClientSocket _connection;
		private readonly MsfClient _client;
		private readonly UnetNetworkManager _unetNetworkManager;
		private string _username;
		private Action _onConnected;

		public ConnectToMaster(
			MsfClient client,
			ClientSettings settings,
			IClientSocket connection,
			BuiltinSignalsProvider builtinSignalsProvider,
			UnetNetworkManager unetNetworkManager
		) {
			_client = client;
			_settings = settings;
			_connection = connection;
			_builtinSignalsProvider = builtinSignalsProvider;
			_unetNetworkManager = unetNetworkManager;
		}

		// Master
		public void Connect(string username, Action onConnected) {
			_onConnected = onConnected;
			_username = string.IsNullOrEmpty(username) ? "Guest" + Random.Range(0, 100000) : username;
			_connection.Connect(_settings.ServerIp, _settings.ServerPort);
			_connection.Connected += Connected;
			_connection.Disconnected += Disconnected;
			D.Log("[ConnectToMaster]", "Connect");
		}

		public void Cancel() {
			// todo как задесконектится
			D.Log("[ConnectToMaster]", "Disconnect");
			_connection.Disconnect();
			_connection.Connected -= Connected;
			_connection.Disconnected -= Disconnected;
		}

		public void RequestFor(string kind) {
			// todo wait a second, then send message...
			D.Log("[ConnectToMaster]", "RequestAGame");
			var msg = MessageHelper.Create((short) CustomOpCodes.RequestStartGame, kind);
			_connection.Peer.SendMessage(msg);
		}

		private void Connected() {
			D.Log("[ConnectToMaster]", "Connected");
			_onConnected?.Invoke();
			_client.Auth.LogInAsGuest(OnLoggedIn, _username);
		}

		private void Disconnected() {
			D.Log("[ConnectToMaster]", "DisConnected");
			_builtinSignalsProvider.StartCoroutine(CoroutineUtils.StartWaiting(2f, GoToState_Stop));
		}

		private void OnLoggedIn(AccountInfoPacket success, string error) {
			if (success == null)
				D.Error("[ConnectToMaster]", error);
			else {
				D.Log("[ConnectToMaster]", $"Logged as \"{success.Username}\"");
				_connection.SetHandler((short) CustomOpCodes.GameServerMatchDetails, OnGameServerMatchDetails);
//				_coroutineProvider.StartCoroutine(CoroutineUtils.StartWaiting(3f, RequestAGame));
			}
		}

//		private void RequestAGame() {
//			// todo wait a second, then send message...
//			D.Log("[ConnectToMaster]","RequestAGame");
//			var msg = MessageHelper.Create((short) CustomOpCodes.RequestStartGame, "Please");
//			_connection.Peer.SendMessage(msg);
//		}

		private void OnGameServerMatchDetails(IIncommingMessage message) {
			D.Log("[ConnectToMaster]", "OnGameServerMatchDetails");
			var details = message.Deserialize(new GameServerMatchDetailsPacket());
			SetupGameServerSocket();
			_gameServerSocket.Connect(details.MachineId, details.AssignedPort);
			D.Log("[ConnectToMaster]", "Start ConnectToGameServer", details.MachineId, details.GamePort);
			_unetNetworkManager.ClientConnect(details.MachineId, details.GamePort);
		}

		private void SetupGameServerSocket() {
			D.Log("[ConnectToMaster]", "SetupGameServerSocket");
			_gameServerSocket = new ClientSocketUnet();
			_gameServerSocket.Connected += ConnectedToGameServer;
			_gameServerSocket.Disconnected += DisconnectedFromGameServer;
			_gameServerSocket.SetHandler(new PacketHandler(0, HandleGameServerMessage));
		}

		private void ConnectedToGameServer() {
			D.Log("[ConnectToMaster]", "Connected to game server");
			_unetNetworkManager.onlineScene = "MultiplayerPVP";
		}

		private void HandleGameServerMessage(IIncommingMessage msg) {
			D.Log("[ConnectToMaster]", "HandleGameServerMessage", msg.ToString());
			var s = msg.ToString();
			if (string.Compare(s, "Start Game", StringComparison.Ordinal) == 0)
				GameStarted();
			else if (string.Compare(s, "End Game", StringComparison.Ordinal) == 0)
				GameEnded();
		}

		private void DisconnectedFromGameServer() {
			D.Log("[ConnectToMaster]", "DisconnectedFromGameServer");
			_gameServerSocket.Connected -= ConnectedToGameServer;
			_gameServerSocket.Disconnected -= DisconnectedFromGameServer;
			_gameServerSocket = null;

			if (!Msf.Connection.IsConnected) {
				_builtinSignalsProvider.StartCoroutine(CoroutineUtils.StartWaiting(2f, GoToState_Stop));
			}
		}

		private void GoToState_Stop() { D.Log("[ConnectToMaster]", "STOP"); }

		//UnetClientController
		private void GameStarted() { D.Log("[ConnectToMaster]", "GameStarted"); }

		private void GameEnded() {
			D.Log("[ConnectToMaster]", "GameEnded");
			_unetNetworkManager.ClientDisconnect();
		}
	}
}
