using System;
using System.Collections.Generic;
using System.Linq;
using Barebones.Logging;
using Barebones.MasterServer;
using Barebones.Networking;
using MasterServer.Enums;
using MasterServer.Models;
using MasterServer.Packets;
using UnityEngine;
using Zenject;

namespace MasterServer {
	public class Matchmaker : IServerModule, ITickable {
		private readonly MatchmakerSettings _settings;
		private readonly BmLogger _logger = Msf.Create.Logger(typeof(Matchmaker).Name);
		private readonly List<Type> _dependencies = new List<Type>();
		private readonly List<Type> _optionalDependencies = new List<Type>();

		public IEnumerable<Type> Dependencies => _dependencies;
		public IEnumerable<Type> OptionalDependencies => _optionalDependencies;
		public ServerBehaviour Server { get; set; }


		private bool _initialized;
		private float _next;
		private float _nextMatch;
		private float _current;

//		public event Action<int> WaitingPlayersChanged;
//		public event Action<int> GamesUnderwayChanged;
//		public event Action<int> GamesPlayedChanged;
//		public event Action<int> GamesAbortedChanged;

		private Dictionary<string, MatchmakerPlayer> _playersWaiting;
		private readonly List<MatchWaiting> _waitingMatches = new List<MatchWaiting>();
		private readonly List<MatchWaiting> _removeWaitingMatches = new List<MatchWaiting>();
		private IServer _server;
		private Dictionary<int, Match> _matches;
		private SpawnersModule _spawnersModule;

		private int _gamesPlayed;

		private int GamesPlayed {
			get => _gamesPlayed;
			set {
				_gamesPlayed = value;
//				GamesPlayedChanged?.Invoke(_gamesPlayed);
			}
		}


		private int _gamesUnderway;

		private int GamesUnderway {
			get => _gamesUnderway;
			set {
				_gamesUnderway = value;
//				GamesUnderwayChanged?.Invoke(_gamesUnderway);
			}
		}

		private int _gamesAborted;

		private int GamesAborted {
			get => _gamesAborted;
			set {
				_gamesAborted = value;
//				GamesAbortedChanged?.Invoke(_gamesAborted);
			}
		}

		public Matchmaker(LogLevelSettings logLevel, MatchmakerSettings settings) {
			_logger.LogLevel = logLevel.Current;
			_settings        = settings;
		}

		public void Initialize(IServer server) {
			_initialized = true;
			_server      = server;

			_playersWaiting = new Dictionary<string, MatchmakerPlayer>();

			_matches = new Dictionary<int, Match>();

			GamesPlayed   = 0;
			GamesAborted  = 0;
			GamesUnderway = 0;

			_server.SetHandler((short) CustomOpCodes.RequestStartGame,          RequestStartGame);
			_server.SetHandler((short) CustomOpCodes.GameServerMatchDetails,    RegisterGameServerDetails);
			_server.SetHandler((short) CustomOpCodes.GameServerMatchCompletion, GameServerMatchCompletion);

			_spawnersModule = _server.GetModule<SpawnersModule>();
			_logger.Info("Initialized");
		}

		public void Tick() {
			if (!_initialized)
				return;
			_current += Time.deltaTime;
			if (_current - _next > 0f && _waitingMatches.Count == 0) {
				D.Log("[Matchmaker] TryToStartAMatch", _current);
				var started = TryToStartAMatch();
				_next = _current + (started ? .1f : _settings.PollFrequency);
			}
			
			if (_waitingMatches.Count == 0 || _current - _nextMatch < 0f)
				return;
			
			D.Log("[Matchmaker] _waitingMatches", _current.ToString("F2"));
			foreach (var match in _waitingMatches) {
				AddPlayersToMatch(match);
				if (match.BeginAt > _current && match.Players.Count < match.MaxPlayers)
					continue;
				_removeWaitingMatches.Add(match);
			}

			foreach (var match in _removeWaitingMatches) {
				D.Log("[Matchmaker] _removeWaitingMatches", _current);
				StartMatch(match);
				_waitingMatches.Remove(match);
			}
			_removeWaitingMatches.Clear();

			_nextMatch = _current + _settings.MatchFrequency;
		}

		private void AddPlayersToMatch(MatchWaiting match) {
			D.Log("[Matchmaker]", "AddPlayersToMatch");
			lock (_playersWaiting) {
				while (_playersWaiting.Count > 0 && match.Players.Count < match.MaxPlayers) {
					if (_playersWaiting.Count > 0) {
						var user   = _playersWaiting.Keys.First();
						var player = _playersWaiting[user];
						RemoveMatchmakerPlayer(user);
						match.Players.Add(player);
					}
				}
			}
		}

		private bool TryToStartAMatch() {
			if (_playersWaiting.Count < _settings.PlayersPerMatch)
				return false;

			_logger.Info($"Matchmaker {_playersWaiting.Count}");

			var match         = new MatchWaiting(_settings.MaxPlayersPerMatch, _current + _settings.WaitForFull);
			var numberPlayers = 0;
			lock (_playersWaiting) {
				while (true) {
					var user   = _playersWaiting.Keys.First();
					var player = _playersWaiting[user];
					RemoveMatchmakerPlayer(user);
					match.Players.Add(player);
					numberPlayers++;
					if (numberPlayers >= _settings.PlayersPerMatch)
						break;
				}
			}
			_waitingMatches.Add(match);

//			var usersInMatch = new List<MatchmakerPlayer>();
//
//			var numberPlayers = 0;
//			// todo RULE OF THUMB
//			// Подождать пару секунд, и только затем начать сбор игроков?
//			// от 2 до 4 игроков сделать как-то
//			// todo players from A to B
//			lock (_playersWaiting) {
//				while (true) {
//					var user   = _playersWaiting.Keys.First();
//					var player = _playersWaiting[user];
//					RemoveMatchmakerPlayer(user);
//					usersInMatch.Add(player);
//					numberPlayers++;
//					if (numberPlayers >= _settings.PlayersPerMatch)
//						break;
//				}
//			}
//
//			StartMatch(usersInMatch);

			return true;
		}

		private class MatchWaiting {
			public readonly List<MatchmakerPlayer> Players = new List<MatchmakerPlayer>();
			public readonly int MaxPlayers;
			public readonly float BeginAt;

			public MatchWaiting(int maxPlayers, float beginAt) {
				MaxPlayers = maxPlayers;
				BeginAt    = beginAt;
			}
		}

		private void RequestStartGame(IIncommingMessage message) {
			var peerId = message.Peer.Id;
			var peer   = Server.GetPeer(peerId);

			if (peer == null) {
				message.Respond("Peer with a given ID is not in the game", ResponseStatus.Error);
				return;
			}

			var account = peer.GetExtension<IUserExtension>();

			if (account == null) {
				message.Respond("Peer has not been authenticated", ResponseStatus.Failed);
				return;
			}

			AddMatchmakerPlayer(new MatchmakerPlayer(account.Username, message.Peer, Time.time));

			message.Respond("Ok", ResponseStatus.Success);
		}

		private void StartMatch(MatchWaiting match) { StartMatch(match.Players); }

		private bool StartMatch(List<MatchmakerPlayer> usersInMatch) {
			var spawnTask = SpawnGameServer(usersInMatch);

			if (spawnTask == null) {
				_logger.Info("no gameservers available");
				// put the users back in the queue
				foreach (var player in usersInMatch) { AddMatchmakerPlayer(player); }
				return false;
			}

			var match = new Match(spawnTask.SpawnId, usersInMatch);

			_matches.Add(match.SpawnId, match);
//			GamesUnderwayChanged?.Invoke(_matches.Count);

			return true;
		}

		private SpawnTask SpawnGameServer(List<MatchmakerPlayer> usersInMatch) {
			var settings = new Dictionary<string, string> {
				{"Type", "Deathmatch"},
				{"Duration", "600"},
				{"Region", "Earth"},
				{"UserCount", usersInMatch.Count.ToString()}
			};

			var task = _spawnersModule.Spawn(settings);

			if (task == null) {
				_logger.Info("Busy");
				return null;
			}

			return task;
		}

		private void AddMatchmakerPlayer(MatchmakerPlayer player) {
			var added = false;
			lock (_playersWaiting) {
				if (!_playersWaiting.ContainsKey(player.Username)) {
					_playersWaiting.Add(player.Username, player);
					added = true;
				}
//				if (added)
//					WaitingPlayersChanged?.Invoke(_playersWaiting.Count);
			}
		}

		private void RemoveMatchmakerPlayer(string username) {
			var removed = false;
			lock (_playersWaiting) {
				if (_playersWaiting.ContainsKey(username)) {
					_playersWaiting.Remove(username);
					removed = true;
				}
//				if (removed)
//					WaitingPlayersChanged?.Invoke(_playersWaiting.Count);
			}
		}

		private void ReturnPlayersToMatchmaker(List<MatchmakerPlayer> usersInMatch) {
			foreach (var player in usersInMatch) { AddMatchmakerPlayer(player); }
		}

		private void RegisterGameServerDetails(IIncommingMessage message) {
			var details = message.Deserialize(new GameServerMatchDetailsPacket());
			_logger.Info(
				$"RegisterGameServerDetails  SpawnId: {details.SpawnId}  MachineId: {details.MachineId}  AssignedPort: {details.AssignedPort}  SpawnCode: {details.SpawnCode}  GameSecondaryPort: {details.GamePort}");

			if (_matches.ContainsKey(details.SpawnId)) {
				var match = _matches[details.SpawnId];
				match.AssignedPort = details.AssignedPort;
				match.MachineId    = details.MachineId;
				match.SpawnCode    = details.SpawnCode;
				match.GamePort     = details.GamePort;

				GamesUnderway = GamesUnderway + 1;

				NotifyClientsAndStartMatch(match);
			}
			else { _logger.Error($"RegisterGameServerDetails: could not find match with spawnId = {details.SpawnId}"); }
		}

		private void NotifyClientsAndStartMatch(Match match) {
			var msg = Msf.Create.Message(
				(short) CustomOpCodes.GameServerMatchDetails,
				new GameServerMatchDetailsPacket {
					SpawnId      = match.SpawnId,
					MachineId    = match.MachineId,
					AssignedPort = match.AssignedPort,
					SpawnCode    = match.SpawnCode,
					GamePort     = match.GamePort
				});

			foreach (var player in match.Players) { player.Peer.SendMessage(msg); }
		}

		private void GameServerMatchCompletion(IIncommingMessage message) {
			var matchCompletion = message.Deserialize(new GameServerMatchCompletionPacket());
			_logger.Info($"GameServerMatchCompletion {matchCompletion.SpawnId} {matchCompletion.Success}");

			if (_matches.ContainsKey(matchCompletion.SpawnId)) {
				// var match = _matches[matchCompletion.SpawnId];

				_matches.Remove(matchCompletion.SpawnId);
				if (matchCompletion.Success)
					GamesPlayed     = GamesPlayed + 1;
				else GamesAborted = GamesAborted + 1;

				GamesUnderway = GamesUnderway - 1;
			}
		}
	}
}
