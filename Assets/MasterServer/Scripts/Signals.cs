﻿using MasterServer.Enums;
using Zenject;

namespace MasterServer {
	public class OnApplicationQuit : Signal<OnApplicationQuit> { }

	public class SetGameState : Signal<SetGameState, ClientState, string> { }
}
