using System.Collections.Generic;

namespace MasterServer.Models {
	public class Match {
		public int SpawnId;
		public List<MatchmakerPlayer> Players;
		public string MachineId { get; set; }
		public int AssignedPort { get; set; }
		public string SpawnCode { get; set; }
		public int GamePort { get; set; }

		public Match(int spawnId, List<MatchmakerPlayer> players) {
			SpawnId = spawnId;
			Players = players;
			MachineId = string.Empty;
			SpawnCode = string.Empty;
		}
	}
}
