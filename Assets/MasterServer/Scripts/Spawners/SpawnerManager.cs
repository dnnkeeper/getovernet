﻿using Barebones.Logging;
using Barebones.MasterServer;
using Barebones.Networking;

namespace MasterServer.Spawners {
	public sealed class SpawnerManager {
		private readonly ServerSettings _settings;
		private readonly OnApplicationQuit _onApplicationQuit;
		private readonly IClientSocket _connection;
		private readonly Spawner _spawner;

		private bool _isSpawnerStarted;

		private readonly BmLogger _logger = Msf.Create.Logger(typeof(SpawnerBehaviour).Name);

		private readonly string _defaultExecutablePath;

		public SpawnerManager(
			LogLevelSettings logLevelSettings,
			ServerSettings settings,
			OnApplicationQuit onApplicationQuit,
			IClientSocket connection,
			Spawner spawner
		) {
			SpawnerController.Logger.LogLevel = logLevelSettings.Current;
			_logger.LogLevel                  = logLevelSettings.Current;
			_settings                         = settings;
			_onApplicationQuit                = onApplicationQuit;
			_connection                       = connection;
			_spawner                          = spawner;

			// Subscribe to connection event
			_connection.AddConnectionListener(OnConnectedToMaster, true);

			_defaultExecutablePath = Msf.Args.ExtractValue("-spawnerPath", settings.ExecutablePath);
			onApplicationQuit.Listen(OnApplicationQuit);
			D.Log("[SpawnerManager]", "Constructor");
		}

		private void OnConnectedToMaster() {
			D.Log("[SpawnerManager]", "OnConnectedToMaster");
			// If we want to start a spawner (cmd argument was found)
			if (Msf.Runtime.IsEditor) StartSpawner();
			else if (Msf.Args.IsProvided(Msf.Args.Names.StartSpawner))
				StartSpawner();
		}

		private void StartSpawner() {
			// In case we went from one scene to another, but we've already started the spawner
			D.Log("[SpawnerManager]", "StartSpawner", _isSpawnerStarted);
			if (_isSpawnerStarted)
				return;

			_isSpawnerStarted = true;

			var spawnerOptions = new SpawnerOptions {MaxProcesses = _settings.MaxSpawnedProcesses};

			if (Msf.Args.IsProvided(Msf.Args.Names.MaxProcesses))
				spawnerOptions.MaxProcesses = Msf.Args.MaxProcesses;

			_logger.Info("Registering as a spawner with options: \n" + spawnerOptions);
			D.Log("[SpawnerManager]", "Register");
			// 1. Register the spawner
			Msf.Server.Spawners.RegisterSpawner(spawnerOptions,
				(spawner, error) => {
					if (error != null) {
						_logger.Error("Failed to create spawner: " + error);
						return;
					}

					//_spawnerController                          = spawner;
					spawner.DefaultSpawnerSettings.AddWebGlFlag = false;

					// Set to run in batchmode
					if (_settings.DefaultSpawnInBatchmode && !Msf.Args.DontSpawnInBatchmode)
						spawner.DefaultSpawnerSettings.SpawnInBatchmode = true;

					// 2. Set the default executable path
					spawner.DefaultSpawnerSettings.ExecutablePath = Msf.Args.IsProvided(Msf.Args.Names.ExecutablePath)
						? Msf.Args.ExecutablePath
						: _defaultExecutablePath;

					// 3. Set the machine IP
					spawner.DefaultSpawnerSettings.MachineIp =
						Msf.Args.IsProvided(Msf.Args.Names.MachineIp) ? Msf.Args.MachineIp : _settings.DefaultMachineIp;
					// 4. (Optional) Set the method which does the spawning, if you want to
					// fully control how processes are spawned
					spawner.SetSpawnRequestHandler(_spawner.SpawnRequest);
					// 5. (Optional) Set the method, which kills processes when kill request is received
					spawner.SetKillRequestHandler(HandleKillRequest);

					_logger.Info("Spawner successfully created. Id: " + spawner.SpawnerId);
				});
			
			D.Log("[SpawnerManager]", "Register", Msf.Server.Spawners);
		}

		private void HandleKillRequest(int spawnid) { SpawnerController.DefaultKillRequestHandler(spawnid); }

		private void OnApplicationQuit() {
			_onApplicationQuit.Unlisten(OnApplicationQuit);
			SpawnerController.KillProcessesSpawnedWithDefaultHandler();
		}

		private void OnDestroy() {
			//todo
			// Remove listener
			_connection.RemoveConnectionListener(OnConnectedToMaster);
			_onApplicationQuit.Unlisten(OnApplicationQuit);
		}
	}
}
