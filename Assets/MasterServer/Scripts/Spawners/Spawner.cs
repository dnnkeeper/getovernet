﻿using System;
using System.Text;
using System.Threading;
using Barebones.Logging;
using Barebones.MasterServer;
using Barebones.Networking;
using MasterServer.Extensions;

namespace MasterServer.Spawners {
	public sealed class Spawner {
		private static readonly StringBuilder Builder = new StringBuilder();
		private readonly ServerSettings _settings;
		private readonly BmLogger _logger = Msf.Create.Logger(typeof(Spawner).Name);
		private readonly PortAllocator _gamePorts;

		public Spawner(LogLevelSettings logLevelSettings, ServerSettings settings) {
			_settings        = settings;
			_logger.LogLevel = logLevelSettings.Current;

			var startingPort = Msf.Args.ExtractValueInt("-gameStartPort", settings.StartingPort);
			var endingPort   = Msf.Args.ExtractValueInt("-gameEndPort",   settings.EndingPort);

			_logger.Info($"startingPort: {startingPort}  endingPort: {endingPort}");
			_gamePorts = new PortAllocator(startingPort, endingPort);
		}

		public void SpawnRequest(SpawnRequestPacket packet, IIncommingMessage message) {
			_logger.Info($"Spwan request {packet} {message}");
			var controller = Msf.Server.Spawners.GetController(packet.SpawnerId);

			if (string.IsNullOrEmpty(Msf.Args.MasterIp))
				controller.DefaultSpawnerSettings.MasterIp = "127.0.0.1";

			_logger.Debug("Custom spawn handler started handling a request to spawn process");

			if (controller == null) {
				message.Respond("Failed to spawn a process. Spawner controller not found", ResponseStatus.Failed);
				return;
			}

			var port     = Msf.Server.Spawners.GetAvailablePort();
			var gamePort = _gamePorts.GetAvailablePort();

			// Check if we're overriding an IP to master server
			var masterIp = string.IsNullOrEmpty(controller.DefaultSpawnerSettings.MasterIp)
				? controller.Connection.ConnectionIp
				: controller.DefaultSpawnerSettings.MasterIp;

			// Check if we're overriding a port to master server
			var masterPort = controller.DefaultSpawnerSettings.MasterPort < 0
				? controller.Connection.ConnectionPort
				: controller.DefaultSpawnerSettings.MasterPort;

			// Machine Ip
			var machineIp = controller.DefaultSpawnerSettings.MachineIp;

			// Path to executable
			var path = _settings.ExecutablePath;
//			if (string.IsNullOrEmpty(path)) {
//				path = File.Exists(Environment.GetCommandLineArgs()[0])
//					? Environment.GetCommandLineArgs()[0]
//					: System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
//			}
			// In case a path is provided with the request
			if (packet.Properties.ContainsKey(MsfDictKeys.ExecutablePath))
				path = packet.Properties[MsfDictKeys.ExecutablePath];

			// Get the scene name
			var sceneNameArgument = packet.Properties.ContainsKey(MsfDictKeys.SceneName)
				? $"{Msf.Args.Names.LoadScene} {packet.Properties[MsfDictKeys.SceneName]} "
				: "";

			if (!string.IsNullOrEmpty(packet.OverrideExePath)) { path = packet.OverrideExePath; }

			// If spawn in batchmode was set and `DontSpawnInBatchmode` arg is not provided
			var spawnInBatchmode = controller.DefaultSpawnerSettings.SpawnInBatchmode && !Msf.Args.DontSpawnInBatchmode;

			var startProcessInfo = new System.Diagnostics.ProcessStartInfo(path) {
				CreateNoWindow  = false,
				UseShellExecute = false,
				Arguments = " " +
				            Builder.Append(" ")
					            .Append(spawnInBatchmode ? "-batchmode -nographics " : "")
					            .Append(controller.DefaultSpawnerSettings.AddWebGlFlag ? Msf.Args.Names.WebGl + " " : "")
					            .Append(sceneNameArgument)
					            .Append($"{Msf.Args.Names.MasterIp} {masterIp} ")
					            .Append($"{Msf.Args.Names.MasterPort} {masterPort} ")
					            .Append($"{Msf.Args.Names.SpawnId} {packet.SpawnId} ")
					            .Append($"{Msf.Args.Names.AssignedPort} {port} ")
					            .Append($"{Msf.Args.Names.MachineIp} {machineIp} ")
					            .Append($"-logfile {_settings.LogsPath}{gamePort}_{DateTime.Now.ToFileTimeUtc()/100000000}.txt ")
					            .Append($"-gamePort {gamePort} ")
					            .Append(Msf.Args.DestroyUi ? Msf.Args.Names.DestroyUi + " " : "")
					            .Append($"{Msf.Args.Names.SpawnCode} \"{packet.SpawnCode}\" ")
					            .Append(packet.CustomArgs)
			};
			Builder.Clear();

			_logger.Debug("Starting process with args: " + startProcessInfo.Arguments);

			var processStarted = false;

			try {
				new Thread(() => {
					try {
						_logger.Debug("New thread started");

						using (var process = System.Diagnostics.Process.Start(startProcessInfo)) {
							_logger.Debug("Process started. Spawn Id: " + packet.SpawnId + ", pid: " + process.Id);
							processStarted = true;

							var processId = process.Id;

							// Notify server that we've successfully handled the request
							BTimer.ExecuteOnMainThread(() => {
								message.Respond(ResponseStatus.Success);
								controller.NotifyProcessStarted(packet.SpawnId, processId, startProcessInfo.Arguments);
							});

							process.WaitForExit();
						}
					}
					catch (Exception e) {
						if (!processStarted)
							BTimer.ExecuteOnMainThread(() => { message.Respond(ResponseStatus.Failed); });

						_logger.Error(
							"An exception was thrown while starting a process. Make sure that you have set a correct build path. " +
							"We've tried to start a process at: '" +
							path +
							"'. You can change it at 'SpawnerBehaviour' component");
						_logger.Error(e);
					}
					finally {
						BTimer.ExecuteOnMainThread(() => {
							// Release the port number
							Msf.Server.Spawners.ReleasePort(port);
							_gamePorts.ReleasePort(gamePort);

							_logger.Debug("Notifying about killed process with spawn id: " + packet.SpawnerId);
							controller.NotifyProcessKilled(packet.SpawnId);
						});
					}
				}).Start();
			}
			catch (Exception e) {
				message.Respond(e.Message, ResponseStatus.Error);
				Logs.Error(e);
			}
		}
	}
}
