﻿using Barebones.MasterServer;
using Barebones.Networking;
using MasterServer.Spawners;
using Zenject;

namespace MasterServer {
	public class MasterServerRunner : IInitializable {
		private readonly ServerSettings _settings;
		private readonly MasterServerBehaviour _behaviour;
		private readonly SpawnerManager _spawner;
		private readonly Matchmaker _matchmaker;
		private readonly IClientSocket _connection;

		public MasterServerRunner(
			MasterServerBehaviour behaviour,
			ServerSettings settings,
			SpawnerManager spawner,
			Matchmaker matchmaker,
			IClientSocket connection
		) {
			_settings   = settings;
			_behaviour  = behaviour;
			_spawner    = spawner;
			_matchmaker = matchmaker;
			_connection = connection;
		}

		public void Initialize() {
			_behaviour.gameObject.SetActive(true);
			_behaviour.AddModule(_matchmaker);
			_behaviour.StartServer();
			if (string.IsNullOrEmpty(Msf.Args.MasterIp))
				_connection.Connect(_settings.Ip, _settings.Port);
			else
				_connection.Connect(Msf.Args.MasterIp, Msf.Args.MasterPort);
			D.Log("[MasterServerRunner]","Initialize");
		}
	}
}
