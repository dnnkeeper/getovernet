﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloner : MonoBehaviour {

    public void Clone()
    {
        GameObject clone = GameObject.Instantiate(gameObject, transform.position, transform.rotation, transform.parent); ;
        clone.SetActive(true);
    }
}
