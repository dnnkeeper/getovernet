﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum DamageType
{
    unknown,
    bludgeoning,
    slashing,
    piercing,
    fire,
    cold,
    acid,
    electrical,
    magical
}

[System.Serializable]
public struct DamageInfo
{
    public float amount;
    public DamageType type;
    public GameObject damageDealer;
    public DamageInfo(float amount, DamageType type, GameObject damageDealer)
    {
        this.amount = amount;
        this.type = type;
        this.damageDealer = damageDealer;
    }
}

public class DamageCollider : MonoBehaviour
{
    public float threshold = 0f;

    public DamageType damageType = DamageType.bludgeoning;

    public float damageAmount = 1f;
    
    public void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("OnCollisionEnter with " + collision.rigidbody.gameObject);

        float magnitude = collision.relativeVelocity.magnitude;

        if (collision.rigidbody != null && magnitude >= threshold)
        {
            collision.collider.SendMessage("OnDamage", new DamageInfo(damageAmount * magnitude, damageType, collision.collider.gameObject), SendMessageOptions.DontRequireReceiver);
        }
    }

    
}
