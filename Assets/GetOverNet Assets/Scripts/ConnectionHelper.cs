﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ConnectionHelper : MonoBehaviour {

    NetworkManager netManager;

    public string adress = "nnseva.noip.me";

	// Use this for initialization
	void Start () {
        netManager = NetworkManager.singleton;
    }

    public void Connect()
    {
        netManager.networkAddress = adress;
        netManager.StartClient();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
