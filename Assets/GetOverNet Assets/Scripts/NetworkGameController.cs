﻿using MonsterLove.StateMachine;
using System.Collections;
using System.Collections.Generic;
using GetOverNet.Interfaces;
using MasterServer.Unet;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NetworkGameController : NetworkBehaviour {

    public enum GameState
    {
        Initial,
        Waiting,
        Prepare,
        InProgress,
        Ended
    }

    protected StateMachine<GameState> fsm;
    
    [SyncVar]
    public GameState currentState;

    [SyncVar, SerializeField]
    protected int playersCount;

    public GameMode gameMode;

    public GameObject notificationPrefab;

    public Text notificationText;

    protected string notificationTextDefault;

    protected Dictionary<int, Transform> playersSpawnPositions = new Dictionary<int, Transform>();

    protected virtual void Awake()
    {
        Debug.Log("NetworkGameController AWAKE");
        
        fsm = StateMachine<GameState>.Initialize(this);

        if (notificationText == null)
        {
            GameObject notificationUI = transform.gameObject;
            if (notificationPrefab != null)
            {
                notificationUI = GameObject.Instantiate(notificationPrefab, transform);
                notificationUI.name = notificationPrefab.name;
            }
            notificationText = notificationUI.GetComponentInChildren<Text>();
        }

        if ( NetworkManager.singleton == null)
        {
            var defaultSceneLoadingOperation = SceneManager.LoadSceneAsync(0, LoadSceneMode.Additive);

            Debug.LogWarning("NetworkManager.singleton == null. Trying to load scene #0 !");
            
            defaultSceneLoadingOperation.completed += OnOfflineSceneLoaded;
        }
        else
        {
            InitializeController();
        }
        
        //StartCoroutine(DO_WaitForNetManager());
    }

    protected virtual void OnOfflineSceneLoaded(AsyncOperation op)
    {
        Debug.LogWarning("Default scene has been loaded. Trying to StartHost()");

        var currentSceneName = SceneManager.GetActiveScene().name;

        NetworkManager.singleton.onlineScene = currentSceneName;

        Debug.Log("new online scene = " + currentSceneName);

        NetworkManager.singleton.StartHost();

        SceneManager.UnloadSceneAsync(0);
        
        InitializeController();
    }

    protected virtual void InitializeController() {
        var netManager    = NetworkManager.singleton;
        
        var eventListener = (IServerPlayerEvents) netManager;
        eventListener.OnServerAddPlayerEvent    += OnServerAddPlayer;
        eventListener.OnServerRemovePlayerEvent += OnServerRemovePlayer;

        string currentSceneName = SceneManager.GetActiveScene().name;

        netManager.onlineScene = currentSceneName;

        netManager.OnServerSceneChanged(currentSceneName);
        //ServerChangeScene
        if (gameMode == null) {
            Debug.LogWarning("GameMode was not assigned. Loading DefaultGameMode...");
            gameMode = Resources.Load<GameMode>("GameModes/DefaultGameMode");
            if (gameMode == null)
                Debug.LogError("Resources/GameModes/DefaultGameMode could not be loaded!");
        }
        Debug.Log("NetworkGameController initialized successfully");
    }

    private void OnNewClientConnected(NetworkConnection obj)
    {
        
    }

    private void OnClientConnectedToServer(NetworkConnection obj)
    {

    }

    protected virtual void Start()
    {
        if (isServer)
            fsm.ChangeState(GameState.Initial);
    }

    protected virtual void Update()
    {
        if (isServer)
        {
            currentState = fsm.State;
        }
        else
        {
            fsm.ChangeState(currentState);
        }
    }

    protected virtual IEnumerator Initial_Enter()
    {
        while (!enabled)
            yield return null;

        gameMode.progress = 0f;

        yield return null;

        if (isServer)
        {
            fsm.ChangeState(GameState.Waiting);
        }
    }

    Coroutine timerRoutine;

    protected virtual void Waiting_Enter()
    {
        if (notificationText != null && !string.IsNullOrEmpty(notificationTextDefault))
            notificationText.text = notificationTextDefault;
    }

    protected virtual void Waiting_Update()
    {
        if (isServer)
        {
            playersCount = NetworkManager.singleton.numPlayers;
            if (playersCount >= gameMode.minPlayers)
            {
                ServerStartGame();
            }
            else
            {
                UpdateNotificationUI(notificationTextDefault + " " + playersCount + " / " + gameMode.minPlayers);
            }
        }
    }

    protected virtual IEnumerator Prepare_Enter()
    {
        if (isServer)
        {
            foreach (NetworkConnection conn in NetworkServer.connections)
            {
                if (conn != null)
                {
                    var playerObject = conn.playerControllers[0].gameObject;
                    if (playerObject != null)
                    {
                        Transform playerStart = playerObject.transform;
                        if (playersSpawnPositions.TryGetValue(conn.connectionId, out playerStart))
                        {
                            playerObject.transform.position = playerStart.position;
                            playerObject.transform.rotation = playerStart.rotation;
                            playerObject.SendMessage("Restart", SendMessageOptions.DontRequireReceiver);
                        }
                    }

                }
            }
        }

        if (timerRoutine != null)
            StopCoroutine(timerRoutine);
        timerRoutine = StartCoroutine(countDownRoutine(5f, x => { OnTimerEnded(x); }, x => { UpdateNotificationUI(x.ToString("F1")); }));
        yield return timerRoutine;
    }

    protected virtual void OnTimerEnded(bool gameStarted)
    {
        if (isServer)
        {
            if (gameStarted)
                fsm.ChangeState(GameState.InProgress);
            else
                fsm.ChangeState(GameState.Waiting);
        }
    }

    protected virtual void InProgress_Enter()
    {
        Debug.Log("GameStarted!");
        
        DisableNotificationUI();

        OnGameStarted();
    }

    protected virtual void InProgress_Update()
    {
        if (isServer)
        {
            if (gameMode.CheckGameProgress() >= 1f)
            {
                fsm.ChangeState(GameState.Ended);
            }
        }
        else
            fsm.ChangeState(currentState);
    }

    protected virtual void OnGameStarted()
    {
        if (isServer)
        {
            foreach (NetworkConnection conn in NetworkServer.connections)
            {
                if (conn != null)
                    StartPlayerObject(conn.playerControllers[0].gameObject);
            }
        }
    }

    protected virtual void StartPlayerObject(GameObject playerObject)
    {
        gameMode.OnPlayerStarted(playerObject);
    }

    protected IEnumerator Ended_Enter()
    {
        if (isServer)
        {
            foreach (NetworkConnection conn in NetworkServer.connections)
            {
                if (conn != null)
                    gameMode.OnPlayerStopped(conn.playerControllers[0].gameObject);
            }
        }
        
        UpdateNotificationUI("Game Over!");

        if (timerRoutine != null)
            StopCoroutine(timerRoutine);

        timerRoutine = StartCoroutine(countDownRoutine(5f, x => { }, x => { UpdateNotificationUI(winnedPlayerObject.name + " has won! " + x.ToString("F1")); }));
        yield return timerRoutine;

        if (isServer)
        {
            fsm.ChangeState(GameState.Initial);
        }
    }

    protected virtual IEnumerator Ended_Exit()
    {
        yield return null;
    }

    protected virtual void ServerAddPlayer(NetworkConnection conn, short playerControllerId, GameObject prefab)
    {
        if (prefab == null)
        {
            if (LogFilter.logError) { Debug.LogError("The PlayerPrefab is empty on the NetworkManager. Please setup a PlayerPrefab object."); }
            return;
        }

        if (prefab.GetComponent<NetworkIdentity>() == null)
        {
            if (LogFilter.logError) { Debug.LogError("The PlayerPrefab does not have a NetworkIdentity. Please add a NetworkIdentity to the player prefab."); }
            return;
        }

        if (playerControllerId < conn.playerControllers.Count && conn.playerControllers[playerControllerId].IsValid && conn.playerControllers[playerControllerId].gameObject != null)
        {
            if (LogFilter.logError) { Debug.LogError("There is already a player at that playerControllerId for this connections."); }
            NetworkServer.DestroyPlayersForConnection(conn);
            //return;
        }

        GameObject player;
        Transform startPos = NetworkManager.singleton.GetStartPosition();

        if (!playersSpawnPositions.ContainsKey(conn.connectionId))
            playersSpawnPositions.Add(conn.connectionId, startPos);

        if (startPos != null)
        {
            player = (GameObject)Instantiate(prefab, startPos.position, startPos.rotation);
        }
        else
        {
            player = (GameObject)Instantiate(prefab, Vector3.zero, Quaternion.identity);
        }

        player.name = "Player " + NetworkManager.singleton.numPlayers;

        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);

        var playerController = conn.playerControllers[playerControllerId];

        OnServerCreatedPlayer(playerController, player);
    }

    public virtual void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        if (gameMode == null)
        {
            ServerAddPlayer(conn, playerControllerId, NetworkManager.singleton.playerPrefab);
        }
        else
        {
            ServerAddPlayer(conn, playerControllerId, gameMode.playerPrefab);
        }

        //Debug.Log("OnServerAddPlayer");
    }
    
    public virtual void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
    {
        if (player.gameObject != null)
        {
            NetworkServer.Destroy(player.gameObject);
        }
    }

    protected virtual void ServerStartGame()
    {
        //Rpc_StartGame();
        StartGame();
    }

    protected virtual void ServerEndGame()
    {
        //Rpc_EndGame();
        EndGame();
    }
    
    /*[ClientRpc]
    protected virtual void Rpc_StartGame()
    {
        StartGame();
    }*/

    protected virtual void StartGame()
    {
        fsm.ChangeState(GameState.Prepare);
    }

    /*[ClientRpc]
    protected virtual void Rpc_EndGame()
    {
        EndGame();
    }*/

    protected virtual void EndGame()
    {
        fsm.ChangeState(GameState.Ended);
    }

    protected GameObject winnedPlayerObject;

    public virtual void GameEndTriggered(GameObject playerObject)
    {
        if (currentState == GameState.InProgress)
        {
            if (isServer)
            {
                winnedPlayerObject = playerObject.transform.root.gameObject;

                RpcSetWinner(winnedPlayerObject.GetComponent<NetworkIdentity>().netId);
                
                SetGameProgress(1f);

                RpcSetGameProgress(1f);
            }
        }
    }

    [ClientRpc]
    protected virtual void RpcSetWinner(NetworkInstanceId instanceID)
    {
        winnedPlayerObject = ClientScene.FindLocalObject(instanceID);
    }

    [ClientRpc]
    protected virtual void RpcSetGameProgress(float p)
    {
        SetGameProgress(p);
    }

    public virtual void SetGameProgress(float p)
    {
        gameMode.progress = p;
    }

    protected virtual void UpdateNotificationUI(string notification)
    {
        if (notificationText != null)
        {
            notificationText.gameObject.SetActive(true);

            if (string.IsNullOrEmpty(notificationTextDefault))
                notificationTextDefault = notificationText.text;

            notificationText.text = notification;
        }
    }

    protected virtual void DisableNotificationUI()
    {
        if (notificationText != null)
            notificationText.gameObject.SetActive(false);
    }    

    protected virtual void OnServerCreatedPlayer(PlayerController playerController, GameObject playerObject)
    {
        gameMode.OnPlayerCreated(playerObject);

        if (currentState == GameState.InProgress)
        {
            StartPlayerObject(playerObject);
        }
    }


    protected virtual IEnumerator countDownRoutine(float timer, System.Action<bool> onCompletedAction = null, System.Action<float> onUpdateAction = null)
    {
        while (timer > 0f)
        {
            timer -= Time.deltaTime;
            onUpdateAction.Invoke(timer);
            yield return null;
        }
        if (onCompletedAction != null)
            onCompletedAction.Invoke(true);
        yield return true;
    }
}
