﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "GameMode", menuName = "GameMode/Default", order = 1)]
public class GameMode : ScriptableObject {
    
    public int minPlayers = 2;

    //public int maxPlayers = 2;

    public GameObject playerPrefab;

    public string onlineSceneName;

    public float progress;
    
    public virtual void OnPlayerCreated(GameObject playerObject)
    {
        FreezePlayer(playerObject, true);
    }

    public virtual void OnPlayerStarted(GameObject playerObject)
    {
        FreezePlayer(playerObject, false);
    }

    public virtual void OnPlayerStopped(GameObject playerObject)
    {
        FreezePlayer(playerObject, true);
    }

    public virtual float CheckGameProgress()
    {
        return progress;
    }

    public virtual bool SaveGameProgress()
    {
        //TODO
        return false;
    }

    public virtual bool LoadGameProgress()
    {
        //TODO
        return false;
    }

    public virtual void FreezePlayer(GameObject playerObject, bool b)
    {
        playerObject.SendMessage("Freeze", b, SendMessageOptions.DontRequireReceiver);
    }
}
