﻿using System;
using GetOverNet.Interfaces;
using UnityEngine.Events;
using UnityEngine.Networking;

public class NetworkManagerExposed : NetworkManager, IServerPlayerEvents {
	// Ui Events
	public UnityEvent onServerStartEvent;
	public UnityEvent onStopServerEvent;
	public UnityEvent onStartClientEvent;
	public UnityEvent onStopClientEvent;

	public event Action<NetworkConnection> OnServerConnectEvent;
	public event Action<NetworkConnection> OnClientConnectEvent;
	public event Action<NetworkConnection, short> OnServerAddPlayerEvent;
	public event Action<NetworkConnection, PlayerController> OnServerRemovePlayerEvent;

	public override void OnServerConnect(NetworkConnection conn) {
		base.OnServerConnect(conn);
		OnServerConnectEvent?.Invoke(conn);
	}

	public override void OnClientConnect(NetworkConnection conn) {
		base.OnClientConnect(conn);
		OnClientConnectEvent?.Invoke(conn);
	}

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId) {
		OnServerAddPlayerEvent?.Invoke(conn, playerControllerId);
	}

	public override void OnServerAddPlayer(NetworkConnection conn,
		short playerControllerId,
		NetworkReader extraMessageReader) {
		OnServerAddPlayerEvent?.Invoke(conn, playerControllerId);
	}

	public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player) {
		OnServerRemovePlayerEvent?.Invoke(conn, player);
	}

	public override void OnStartServer() {
		base.OnStartServer();
		onServerStartEvent.Invoke();
	}

	public override void OnStopServer() {
		base.OnStopServer();
		onStopServerEvent.Invoke();
	}

	public override void OnStartClient(NetworkClient client) {
		base.OnStartClient(client);

		onStartClientEvent.Invoke();
	}

	public override void OnStopClient() {
		base.OnStopClient();

		onStopClientEvent.Invoke();
	}
	
	// For ui events

	public void StartHostVoid() { StartHost(); }

	public void StartServerVoid() { StartServer(); }

	public void StartClientVoid() { StartClient(); }

	public void Disconnect() {
		if (NetworkServer.active) {
			if (IsClientConnected()) { StopHost(); }
			else
				StopServer();
		}
		else { StopClient(); }
	}
}
