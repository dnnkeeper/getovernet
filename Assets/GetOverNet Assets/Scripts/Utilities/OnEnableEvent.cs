﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnEnableEvent : MonoBehaviour {

    public UnityEvent onEnable;
    public UnityEvent onDisable;

    void OnEnable()
    {
        if (enabled) {
            onEnable.Invoke();
        }
    }

    void OnDisable()
    {
        if (enabled)
        {
            onDisable.Invoke();
        }
    }
}
