﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TabbedMenuElement : MonoBehaviour {

    public UnityEvent onSelected;
    public UnityEvent onDeselected;

    void OnTabSelect(bool b)
    {
        if (!b)
        {
            //Debug.Log(gameObject + " deselected");
            onDeselected.Invoke();
        }
        else
        {

            //Debug.Log(gameObject + " selected");
            onSelected.Invoke();
        }
    }
}
