﻿using System;
using UnityEngine.Networking;

namespace GetOverNet.Interfaces {
	public interface IServerPlayerEvents {
		event Action<NetworkConnection, short> OnServerAddPlayerEvent;
		event Action<NetworkConnection, PlayerController> OnServerRemovePlayerEvent;
	}
}
