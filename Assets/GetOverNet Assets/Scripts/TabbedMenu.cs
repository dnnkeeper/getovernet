﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TabbedMenu : MonoBehaviour {

    public int selectedBtn;

    public Button.Transition transition;

    int activatedBtn = -1;

    public Button[] buttons = new Button[0];

    //public UnityEvent onClick;

    public UnityEvent[] buttonEvents;
    
    void Awake()
    {
        //Debug.LogWarning("be aware Input.multiTouchEnabled = false");
        //Input.multiTouchEnabled = false;
        
        if (buttons.Length == 0)
            buttons = GetComponentsInChildren<Button>();
        //buttonEvents = new UnityEvent[buttons.Length];
        //Debug.Log("buttons " + buttons.Length);

        for (int i = 0; i < buttons.Length; i++)
        {
            int n = i;

            DoSelectTransition(buttons[i], false);

            buttons[i].onClick.AddListener( delegate{ OnClickEvent(n); } );
            
        }
    }

    private void OnEnable()
    {
        SelectAndInvoke(selectedBtn);
    }

    void OnClickEvent(int n)
    {
        //Debug.Log("OnClickEvent " + n);
        //onClick.Invoke();
        //selectedBtn = n;
        //if (n < buttons.Length)
        //    buttonEvents[n].Invoke();
        if (n != selectedBtn)
            SelectAndInvoke(n);
    }

    public void InvokeSelected()
    {
        buttonEvents[selectedBtn].Invoke();
    }

    public void SelectAndInvoke(int n)
    {
        Select(n);

        if (buttonEvents.Length > n)
            buttonEvents[n].Invoke();

        for (int i = 0; i < buttons.Length; i++)
        {
            if (i != n)
            {
                DoSelectTransition(buttons[i], false);
                buttons[i].SendMessage("OnTabSelect", false, SendMessageOptions.DontRequireReceiver);
            }
            else
            {
                DoSelectTransition(buttons[i], true);
                buttons[i].SendMessage("OnTabSelect", true, SendMessageOptions.DontRequireReceiver);
            }
        }
    }

    public void Select(int n)
    {
        if (selectedBtn != n)
        {
            selectedBtn = n;

            Debug.Log("Select " + selectedBtn + ": " + buttons[selectedBtn].name);
        }
    }

    public bool useHighlatedSprite = true;

	// Update is called once per frame
	void Update () {
        if (selectedBtn >= 0 && buttons.Length > selectedBtn)
        {
            if (activatedBtn != selectedBtn)
            {
                //Debug.Log("Update button");
                if (activatedBtn >= 0)
                {
                    //buttons[activatedBtn].image.sprite = buttons[activatedBtn].spriteState.disabledSprite;
                    //DoSelectTransition(buttons[activatedBtn], false);
                }

                activatedBtn = selectedBtn;

                //buttons[selectedBtn].image.sprite = useHighlatedSprite? buttons[selectedBtn].spriteState.highlightedSprite : buttons[selectedBtn].spriteState.pressedSprite;
                //DoSelectTransition(buttons[selectedBtn], true);
            }
        }
    }

    void DoSelectTransition(Button btn, bool active)
    {
        switch (btn.transition)
        {
            case Selectable.Transition.None:
                btn.image.color = active ? (useHighlatedSprite ? btn.colors.highlightedColor : btn.colors.pressedColor) : btn.colors.disabledColor;
                break;
            case Selectable.Transition.ColorTint:
                btn.transition = Selectable.Transition.None;
                break;
            case Selectable.Transition.SpriteSwap:
                btn.image.sprite = active? (useHighlatedSprite ? btn.spriteState.highlightedSprite : btn.spriteState.pressedSprite) : btn.spriteState.disabledSprite;
                break;
            case Selectable.Transition.Animation:
                break;
            default:
                break;
        }
    }
}
