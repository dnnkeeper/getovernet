﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionEventTrigger : MonoBehaviour {

    public string triggerTag = "Player";

    public UnityGameObjectEvent onTriggerEnter;

    public UnityGameObjectEvent onTriggerExit;

    public UnityGameObjectEvent onCollisionEnter;

    public UnityGameObjectEvent onCollisionExit;

    public void OnTriggerEnter(Collider other)
    {
        if ( string.IsNullOrEmpty(triggerTag) || other.CompareTag(triggerTag))
        {
            onTriggerEnter.Invoke(other.gameObject);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (string.IsNullOrEmpty(triggerTag) || other.CompareTag(triggerTag))
        {
            onTriggerExit.Invoke(other.gameObject);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (string.IsNullOrEmpty(triggerTag) || collision.rigidbody.CompareTag(triggerTag))
        {
            onCollisionEnter.Invoke(collision.rigidbody.gameObject);
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (string.IsNullOrEmpty(triggerTag) || collision.rigidbody.CompareTag(triggerTag))
        {
            onCollisionExit.Invoke(collision.rigidbody.gameObject);
        }
    }
}
