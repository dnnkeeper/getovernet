﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightPole : MonoBehaviour
{
	bool active = false;

	public GameObject lightStop, lightGo;

	void OnTriggerEnter(Collider col)
	{
		if(!active) {
			lightStop.SetActive(false);
			lightGo.SetActive(true);
		}

		active = true;
	}
}
