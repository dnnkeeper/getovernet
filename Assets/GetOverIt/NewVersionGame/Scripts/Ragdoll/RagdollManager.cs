﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollManager : MonoBehaviour 
{
	public Rigidbody center;

	void OnCollisionEnter(Collision other)
	{
		if(other.transform.tag == "Hammer")
			center.isKinematic = false;
	}
}
