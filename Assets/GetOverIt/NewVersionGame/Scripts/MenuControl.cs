﻿using UnityEngine;

public class MenuControl : MonoBehaviour
{
    //[NotNull]
    public CharacterControl m_characterControl;

    public void SetCharacterProfile(CharacterProfile characterProfile)
    {
        m_characterControl.SetProfile(characterProfile);
    }
}