﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using CodeWriter.Audio;

public class CollisionEffect : MonoBehaviour
{
	// Система частиц и ее параметры
	[Header ("Система частиц для коробки")]
	public ParticleSystem particle;
	
	[Tooltip ("Сколько частиц выпустить при столкновении")]
	public int numParticleCount = 10;
	
	[Tooltip ("Минимальная скорость персонажа при столкновении с препятствием, при котором появятся частицы")]
	[Range(0.1f, 10f)]public float speedCharacter = 5f;
	
	
	// Звук столкновения и его параметры
	//[Space(15)]
	//[Header ("Звук при столкновении")]
	//public AudioEvent audioEvent;
	
	
	// Задержка. Чтобы эффекты не сыпались кучей при скольжении 
	[Header ("Задержка между столкновениями")]
	[Tooltip ("Чтобы эффекты не сыпались кучей при скольжении - включи задержку")]
	public bool delay = true;
	[Range(0.1f, 2f)]public float timerDelay = 0.5f;
	private float timer;
	private bool activeEffect = true;
	
	void Start()
	{
		timer = timerDelay;
	}
	
	void OnCollisionEnter(Collision collision)
    {
		if(activeEffect) {
			if(this.GetComponent<Rigidbody>().velocity.sqrMagnitude > speedCharacter) {
				particle.transform.position = collision.contacts[0].point;
				Quaternion rotate = Quaternion.LookRotation(collision.relativeVelocity);
				particle.transform.rotation = rotate;
				particle.Emit(numParticleCount);

				//Audio.GetPlayer(audioEvent).Play();
				
				if(delay)
					activeEffect = false;
			}
		}
    }
	
	void Update()
	{
		if(!activeEffect) {
			timer -= Time.deltaTime;
			
			if(timer <= 0) {
				timer = timerDelay;
				activeEffect = true;
				return;
			}
		}
	}
}
