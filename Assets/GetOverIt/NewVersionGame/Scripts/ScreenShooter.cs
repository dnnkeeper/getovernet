﻿using UnityEngine;
using System.Collections;

public class ScreenShooter : MonoBehaviour
{
	public string names;
	[Range(0,10)]
    public int SuperScreenMultiplier;
 
    [Header("read only")]
    public Vector2 screenshotResolution;
 
    void Update ()
	{
        if (Input.GetKey(KeyCode.LeftControl))
        {
			screenshotResolution = new Vector2( Camera.main.pixelRect.width * SuperScreenMultiplier, Camera.main.pixelRect.height * SuperScreenMultiplier);
       
            System.DateTime time = System.DateTime.Now;
			string path = "Assets/GetOverIt/Icons_GO/" + names + ".png";
            Debug.Log ("Screenshot captured! " + path);
            ScreenCapture.CaptureScreenshot (path, SuperScreenMultiplier);
        }
    }
}
