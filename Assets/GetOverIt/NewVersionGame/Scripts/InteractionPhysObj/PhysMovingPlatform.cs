﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysMovingPlatform : MonoBehaviour
{
	[Header ("Вкл/выкл")]
	public bool active;

	public enum MotionType {PingPong, OneWay}
	[Header ("Тип движения платформы?")]
	public MotionType motion = MotionType.PingPong;

	public enum TargetType {target_A, target_B}
	[Header ("К какой точке начать движение?")]
	public TargetType target = TargetType.target_A;

	[Header ("Скорость движения")]
	public float speed = 1;

	public Transform targ_A, targ_B;

	float timer = 0;

	void FixedUpdate()
	{
		if(active) {
			if(timer <= 2.0f) {
				timer += Time.deltaTime * speed;
			}
			float lerp = timer / 2.0f;
			lerp = Mathf.Lerp(-1, 1, lerp);
			float softMove = Mathf.Sin(lerp * 90.0f * Mathf.Deg2Rad);
			softMove += 1;
			softMove /= 2;

			switch(target)
			{
			case TargetType.target_A: transform.position = Vector3.Lerp(targ_B.position, targ_A.position, softMove); break;
			case TargetType.target_B: transform.position = Vector3.Lerp(targ_A.position, targ_B.position, softMove); break;
			}

			if(timer >= 2.0f) {
				timer = 0;
				switch(target)
				{
				case TargetType.target_A: target = TargetType.target_B; break;
				case TargetType.target_B: target = TargetType.target_A; break;
				}

				switch(motion)
				{
				case MotionType.OneWay: active = false; break;
				}
			}
		}
	}
}
