﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysPlatformFollow : MonoBehaviour 
{
	public GameObject character;

	public bool playerStay;
	public bool hammerStay;

	void OnTriggerEnter(Collider other)
	{
		if(other.transform.tag == "Player") {
			playerStay = true;
		} else if(other.transform.tag == "Hammer") {
			hammerStay = true;
		}

		if(hammerStay || playerStay) {
			character.transform.parent = this.transform;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.transform.tag == "Player") {
			playerStay = false;
		}

		if (other.transform.tag == "Hammer") {
			hammerStay = false;
		}

		if(!hammerStay && !playerStay) {
			character.transform.parent = null;
		}
	}	
}
