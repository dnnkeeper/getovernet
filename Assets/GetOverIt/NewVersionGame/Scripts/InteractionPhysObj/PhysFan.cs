﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysFan : MonoBehaviour 
{
	[Header ("Вкл/выкл")]
	public bool active;

	public enum DirectionType {Up, Down, Right, Left}
	[Header ("В какую сторону дует ветор?")]
	public DirectionType directionWind = DirectionType.Up;

	public float windStrength = 500;

	[Header ("Сила угасания ветра от расстояния")]
	public AnimationCurve softStrength;

	[Header ("Пропеллер")]
	public GameObject fan;

	public float speedRotate = 5f;

	Vector3 m_Size, m_Min, m_Max;

	void Start () 
	{
		Collider m_collider = GetComponent<Collider>();
		m_Size = m_collider.bounds.size;
		m_Min = m_collider.bounds.min;
		m_Max = m_collider.bounds.max;
		//Debug.Log(m_Size);
		//Debug.Log(m_Min);
		//Debug.Log(m_Max);
	}
	
	void Update () 
	{
		if(active) {
			fan.transform.Rotate(new Vector3(0, 1 * speedRotate, 0));
		}
	}

	void OnTriggerStay(Collider other)
	{
		if(other.transform.tag == "Player") {
			switch(directionWind)
			{
			case DirectionType.Up: other.GetComponent<Rigidbody>().AddForce(new Vector3(0, 1, 0)); break;
			case DirectionType.Left: 
				float posCharacter = other.transform.position.x;
				float x = m_Max.x - m_Min.x;
				float p = m_Max.x - posCharacter;
				float persent = 1 - Mathf.Clamp01(p / x);
				other.GetComponent<Rigidbody>().AddForce(new Vector3(-1 * (windStrength * softStrength.Evaluate(persent)), 0, 0)); 
				break;
			}
		}
	}
}
