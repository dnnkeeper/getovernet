﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysJumper : MonoBehaviour
{
	[Header ("Вкл/выкл")]
	public bool active;

	[Header ("Сила импульса")]
	[Range(10,100)]public float forceJump = 30;

	[Header ("Система частиц")]
	public ParticleSystem particles;

	bool prewActive;

	void Start() 
	{
		prewActive = active;
		OnOff();
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.transform.tag == "Player") {
			if(active) {
				col.transform.GetComponent<Rigidbody>().AddForce(Vector3.up * forceJump * 10000);
			}
		}
	}

	void Update()
	{
		if(active != prewActive) {
			prewActive = active;
			OnOff();
		}
	}

	void OnOff()
	{
		if(active) {particles.Play();}
		else 	   {particles.Stop();}		
	}
}
