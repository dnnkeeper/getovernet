﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysToggleTrigger : MonoBehaviour
{
	[Header ("Вкл/выкл")]
	public bool active;

	//[NotNull]
	[Header ("Объект для управления")]
	public GameObject controlObject;

	private void OnTriggerEnter(Collider other)
	{
		if(other.transform.tag == "Player")
		{
			if(!active) {
				active = true;

				controlObject.GetComponent<PhysMovingPlatform>().active = active;
			}
		}
	}
}
