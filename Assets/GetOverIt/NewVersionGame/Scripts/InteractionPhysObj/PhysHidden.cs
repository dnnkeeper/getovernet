﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysHidden : MonoBehaviour
{
	[Header ("Вкл/выкл")]
	public bool active;

	[Header ("Объект который скрываем/показываем")]
	public GameObject controlObject;

	bool prewActive;

	void Start() 
	{
		prewActive = active;
		OnOff();
	}

	void Update()
	{
		if(active != prewActive) {
			prewActive = active;
			OnOff();
		}
	}

	void OnOff()
	{
		controlObject.SetActive(!active);	
	}
}
