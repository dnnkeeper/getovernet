﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysToggleButton : MonoBehaviour 
{
	public enum ToggleType {OneClick, MultiClick, TimerClick}
	[Header ("Тип кнопки")] 
	public ToggleType ButtonType = ToggleType.OneClick;

	[Header ("Вкл/выкл")]
	public bool active;

	[Header ("Таймер")]
	public float timer = 5f;
	float deltatimer;
	bool timerGate;

	//[NotNull]
	[Header ("Объект для управления")]
	public GameObject controlObject;


	SpringJoint sj;

	bool checkDw;
	bool checkUp;

	int numClick = 0;

	int IDcontrolObject = 0;

	void Start()
	{
		sj = GetComponent<SpringJoint>();

		if(active) {sj.minDistance = 0.2f;}
		else 	   {sj.minDistance = 0.0f;}

		switch(ButtonType)
		{
		case ToggleType.OneClick: active = false; break;
		case ToggleType.TimerClick: active = false; break;
		}

		// Распознование объекта управления
		if(controlObject.GetComponent<PhysJumper>() != null) {
			IDcontrolObject = 1;
		} else if(controlObject.GetComponent<PhysHidden>() != null) {
			IDcontrolObject = 2;
		} else if(controlObject.GetComponent<PhysMovingPlatform>() != null) {
			IDcontrolObject = 3;
		} else {
			Debug.LogError("Не найден объект управления для кнопки! - " + this.transform.parent.name);
		}
	}

	void Update()
	{
		switch(ButtonType)
		{
		case ToggleType.OneClick: if(numClick < 1) {TapButton();} break;
		case ToggleType.MultiClick: TapButton(); break;
		case ToggleType.TimerClick: TapButton(true); break;
		}
	}

	void TapButton(bool useTimer = false)
	{
		if(timerGate) {
			deltatimer -= Time.deltaTime;
			if(deltatimer <= 0) {
				timerGate = false;
				sj.minDistance = 0.0f;
				sj.GetComponent<Rigidbody>().WakeUp();
				active = false;
				SendActivity();
			}
		}

		if(!checkDw && !checkUp) {
			if(!active) {
				if(this.transform.localPosition.y <= -0.65f) {
					sj.minDistance = 0.2f;
					active = true;
					SendActivity();
					checkDw = true;
					numClick++;

					if(useTimer) {
						deltatimer = timer;
						timerGate = true;
					}
				}
			} else {
				if(useTimer) return;
				if(this.transform.localPosition.y <= -0.70f) {
					sj.minDistance = 0.0f;
					active = false;
					SendActivity();
					checkUp = true;
					numClick++;
				}
			}
		}

		if(checkDw) {
			if(this.transform.localPosition.y >= -0.64f) {
				checkDw = false;
			}
		}
		if(checkUp) {
			if(this.transform.localPosition.y >= -0.44f) {
				checkUp = false;
			}
		}
	}

	void SendActivity()
	{
		switch(IDcontrolObject)
		{
		case 0: Debug.LogError("Не найден объект управления для кнопки! - " + this.transform.parent.name); break;
		case 1: controlObject.GetComponent<PhysJumper>().active = active; break;
		case 2: controlObject.GetComponent<PhysHidden>().active = active; break;
		case 3: controlObject.GetComponent<PhysMovingPlatform>().active = active; break;
		}
	}
}
