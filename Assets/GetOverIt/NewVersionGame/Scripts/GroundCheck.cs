﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour 
{
	public static bool grounded;

	Transform transform_m;

	void Start()
	{
		transform_m = GetComponent<Transform>();
	}

	void FixedUpdate () 
	{
		grounded = Physics.Raycast(transform_m.position, Vector3.down, 0.45f);
		//Debug.DrawRay(transform_m.position, Vector3.down);
	}
}
