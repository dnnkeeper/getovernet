﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ConfigurableJoint))]
public class HammerController : MonoBehaviour {

    //[NotNull]
    public Camera mainCamera;
	
	public enum HammerMode
    {
        TranslateChild,
        UseTargetPosition
    }

    ConfigurableJoint _cJoint;

    public float rotationSpeed = 5.0f;

    public float jointSpeed = 10.0f;

    public float minRadius = 0.5f;

    public float maxRadius = 2f;
	
    public HammerMode jointMode;

    public float velocityLimit = 100f;

    Rigidbody _rb;

    Vector3 _childLocalPos;

    Vector3 _clickPos;

    Vector3 _bodyToClickPos;

    public Vector3 mousePos;

    //float timerDelay = 1f;
    //bool delay = true;

    // Use this for initialization
    void Start () {
        
        _rb = GetComponent<Rigidbody>();

        _cJoint = GetComponent<ConfigurableJoint>();

        _bodyToClickPos = _cJoint.connectedAnchor;
        
        _childLocalPos = transform.GetChild(0).transform.localPosition;

        mousePos = _cJoint.transform.position;

        if (mainCamera == null)
            mainCamera = Camera.main;
    }

    

    // Set target position, pos in LOCAL CAMERA SPACE
    public void SetTargetPos(Vector3 pos)
    {
        mousePos = mainCamera.transform.TransformPoint( pos );
    }

    // Set target position in WORLD SPACE
    public void SetTargetPosWorld(Vector3 pos)
    {
        mousePos = pos;
    }

    // Set hammer position in LOCAL CAMERA SPACE
    public void SetHammerPos(Vector3 pos)
    {
        mousePos = mainCamera.transform.TransformPoint(pos);
        UpdateHammer(mousePos, true);
    }

    // Update is called once per frame
    void FixedUpdate() {

        /*if (delay) {
			timerDelay -= Time.deltaTime;
			if(timerDelay <= 0) {
				delay = false;
			}

            mousePos = transform.position - transform.right;
        }*/

        //MOVED TO MOUSE INPUT CONTROL
        //if (FingerUtilities.GetFingerState() == FingerState.None)
        //    return;

        _clickPos = mousePos;

        UpdateHammer(_clickPos);
    }

	void UpdateHammer(Vector3 _clickPos, bool immediateControl = false)
	{
		Vector3 bodyToClickPos = Vector3.ClampMagnitude(_clickPos - _cJoint.connectedBody.transform.position, maxRadius);
		if (bodyToClickPos.sqrMagnitude < minRadius* minRadius ) {
			_bodyToClickPos = _bodyToClickPos.normalized * minRadius;
		}
		_bodyToClickPos = Vector3.Lerp(_bodyToClickPos, bodyToClickPos, jointSpeed * Time.deltaTime);
		_childLocalPos = -(Vector3.up * _bodyToClickPos.magnitude / _cJoint.transform.localScale.y);

		Quaternion rot = Quaternion.Inverse(Quaternion.LookRotation(_cJoint.connectedBody.transform.forward, -_bodyToClickPos.normalized));

		_cJoint.angularZMotion = ConfigurableJointMotion.Free;

        _cJoint.targetRotation = rot;//Quaternion.Lerp(_cJoint.targetRotation, rot, rotationSpeed * Time.deltaTime);
        
        _rb.velocity = Vector3.ClampMagnitude(_rb.velocity, velocityLimit);
        //Debug.DrawRay(_rb.position, _rb.velocity, Color.red);

        if (immediateControl)
        {
            _cJoint.transform.rotation = Quaternion.Inverse(_cJoint.targetRotation);
        }

        switch (jointMode)
		{
		case HammerMode.TranslateChild:

			_cJoint.yMotion = ConfigurableJointMotion.Locked;

			_cJoint.transform.GetChild(0).localPosition = _childLocalPos;

			break;

		case HammerMode.UseTargetPosition:

			_cJoint.yMotion = ConfigurableJointMotion.Free;

			_cJoint.transform.GetChild(0).localPosition = Vector3.zero;

			_cJoint.targetPosition = -(rot * _bodyToClickPos);

            /*if (immediateControl)
            {
                _cJoint.transform.position = _cJoint.targetPosition;
            }*/

            break;

		default:

			break;
		}

		Debug.DrawRay(_cJoint.connectedBody.transform.position, _cJoint.transform.rotation * -Vector3.up , Color.cyan);

		_rb.WakeUp();

		Debug.DrawRay(_cJoint.connectedBody.transform.position, _bodyToClickPos, Color.red);

	}
}
