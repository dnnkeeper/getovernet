﻿using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

public class CharacterControl : MonoBehaviour
{
    public HammerController m_hammer;

    [HideInInspector]
    public Item[] m_characters, m_knives, m_boxes;

    [Serializable]
    public class Item
    {
        public string name;
        public GameObject gameObject;
        public GameObject mask;
    }

    public void SetProfile(CharacterProfile profile)
    {
        ActivateItem(m_characters, profile.Character);
        ActivateItem(m_knives, profile.Knife);
        ActivateItem(m_boxes, profile.Box);
    }

    public void TeleportTo(Vector3 position)
    {
        transform.position = position;
		
		GameObject cam = GameObject.Find("Main Camera");
		cam.transform.position = new Vector3(position.x, position.y, -12);
    }

    internal static void ActivateItem(Item[] items, string name)
    {
        foreach (var item in items)
        {
            if (item.gameObject)
                item.gameObject.SetActive(item.name == name);

            if (item.mask)
                item.mask.SetActive(item.name == name);
        }
    }

    public void ActivateController()
    {
        m_hammer.enabled = true;
    }

    public void DeactivateController()
    {
        m_hammer.enabled = false;
    }
}

public class CharacterProfile
{
    public string Character { get; private set; }
    public string Knife { get; private set; }
    public string Box { get; private set; }

    public CharacterProfile(string character, string knife, string box)
    {
        Character = character;
        Knife = knife;
        Box = box;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(CharacterControl))]
public class MenuControllerEditor : Editor
{
    private ReorderableList m_characters, m_knives, m_boxes;

    private void OnEnable()
    {
        var me = (CharacterControl)target;
        m_characters = CreateList(serializedObject.FindProperty("m_characters"), () => me.m_characters);
        m_knives = CreateList(serializedObject.FindProperty("m_knives"), () => me.m_knives);
        m_boxes = CreateList(serializedObject.FindProperty("m_boxes"), () => me.m_boxes);
    }

    private static ReorderableList CreateList(SerializedProperty property, Func<CharacterControl.Item[]> itemsGetter)
    {
        var list = new ReorderableList(property.serializedObject, property);
        list.elementHeight = EditorGUIUtility.singleLineHeight * 2 + 10;
        list.drawHeaderCallback = rect => GUI.Label(rect, property.displayName);
        list.drawElementCallback = (rect, index, active, focus) =>
        {
            rect.height = EditorGUIUtility.singleLineHeight;

            var prop = list.serializedProperty.GetArrayElementAtIndex(index);
            var nameProp = prop.FindPropertyRelative("name");
            var goProp = prop.FindPropertyRelative("gameObject");
            var maskProp = prop.FindPropertyRelative("mask");

            var nameRect = new Rect(rect) { width = EditorGUIUtility.labelWidth };
            var goRect = new Rect(rect) { xMin = nameRect.xMax, xMax = rect.xMax - 20 };
            var maskRect = new Rect(goRect) { y = rect.y + EditorGUIUtility.singleLineHeight };
            var activateRect = new Rect(rect) { xMin = rect.xMax - 16 };

            EditorGUI.PropertyField(nameRect, nameProp, GUIContent.none);

            var oldColor = GUI.color;
            if (goProp.objectReferenceValue == null) GUI.color = Color.red;
            EditorGUI.PropertyField(goRect, goProp, GUIContent.none);
            GUI.color = oldColor;

            EditorGUI.PropertyField(maskRect, maskProp, GUIContent.none);

            var selected = goProp.objectReferenceValue && ((GameObject)goProp.objectReferenceValue).activeSelf;
            if (selected != GUI.Toggle(activateRect, selected, GUIContent.none))
            {
                CharacterControl.ActivateItem(itemsGetter(), nameProp.stringValue);
            }
        };
        return list;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        m_characters.DoLayoutList();
        m_knives.DoLayoutList();
        m_boxes.DoLayoutList();

        serializedObject.ApplyModifiedProperties();
    }
}
#endif