﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PawnSystem.Network;
using System;
using UnityEngine.Networking;

public class GetOverInputControl : MonoBehaviour, IControls
{ 
    public UnityVector3Event onVectorInputUpdate;

    public UnityVector3Event onStateInputUpdate;

    public Vector3 clickPos = Vector3.up;

    private void Awake()
    {
        //Debug.Log("AWAKE");
        //clickPos = CameraRef.transform.InverseTransformPoint(transform.position + transform.up*2f);
        //EvaluateControls();
        Restart();
    }

    virtual protected void Update() 
    {
        /// Если персонаж запущен без сети, то будет происходить Update, и управление будет обновляться и назначаться. Если подключен компонент сетевой репликации, функция Update отключается, а обновление управления вызывается только у владельца персонажа
        UpdateControls();
        EvaluateControls();
    }

    public Camera CameraRef;

    public void UpdateControls()
    {
        /// Ищем пересечение луча, исходящего из экрана с плоскостью, построенной параллельно плоскости экрана на расстоянии персонажа
        if (CameraRef != null)
        {
            Plane objectPlane = new Plane(CameraRef.transform.forward, transform.position);

            Ray clickRay = CameraRef.ScreenPointToRay(Input.mousePosition);
            if (Input.touches.Length > 0)
            {
                clickRay = CameraRef.ScreenPointToRay(Input.touches[0].position);
            }
                
            float distance = 0f;
            if (objectPlane.Raycast(clickRay, out distance))
            {
                /// переведём полученную точку в локальное пространство камеры чтобы смещение камеры в мировом пространстве, вызванное движением персонажа, смещало вместе с собой и позицию клика
                clickPos = CameraRef.transform.InverseTransformPoint( clickRay.origin + clickRay.direction * distance );
            }
        }
    }

    public void EvaluateControls()
    {
        onVectorInputUpdate.Invoke(clickPos);
    }

    /// <summary>
    /// Если какие-то данные управления приходят как delta, то при рассинхронизации или начальной синхронизации здесь мы должны вызвать установку всех параметров в синхронизированное состояние
    /// </summary>
    public void StateResync()
    {
        clickPos = CameraRef.transform.InverseTransformPoint(transform.position + transform.up);

        onStateInputUpdate.Invoke(clickPos);
    }

    void Restart()
    {
        StateResync();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(CameraRef.transform.TransformPoint(clickPos), 0.1f);
    }
}
