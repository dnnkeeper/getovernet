﻿#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
#define TOUCH_INPUT
#endif

using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class FingerUtilities
{
    public static FingerState GetFingerState()
    {
#if TOUCH_INPUT
            if (Input.touchCount != 1)
                return FingerState.None;

            if (EventSystem.current.IsPointerOverGameObject(0))
                return FingerState.None;

            var touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    return FingerState.Down;

                case TouchPhase.Ended:
                    return FingerState.Up;

                case TouchPhase.Canceled:
                    return FingerState.None;

                case TouchPhase.Moved:
                case TouchPhase.Stationary:
                    return FingerState.Pressed;

                default:
                    throw new InvalidProgramException();
            }
#else
        bool overGameObject = EventSystem.current != null && EventSystem.current.IsPointerOverGameObject();

        const KeyCode keyCode = KeyCode.Space;

        if ((Input.GetMouseButtonDown(0) && !overGameObject) || Input.GetKeyDown(keyCode))
            return FingerState.Down;

        if ((Input.GetMouseButtonUp(0) && !overGameObject) || Input.GetKeyUp(keyCode))
            return FingerState.Up;

        if ((Input.GetMouseButton(0) && !overGameObject) || Input.GetKey(keyCode))
            return FingerState.Pressed;

        return FingerState.None;
#endif
    }
}

public enum FingerState
{
    None,
    Down,
    Pressed,
    Up,
}
