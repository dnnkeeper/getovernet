﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using PawnSystem.Network;
using UnityEngine.UI;

//[System.Serializable]
public struct StateSnapshot
{
    public Vector3 clickPos;
    public Vector3 pos;
    public Vector3 velocity;
    public float timestamp;

    public StateSnapshot(Vector3 clickPosition, Vector3 position, Vector3 velocityVector, float t)
    {
        this.clickPos = clickPosition;
        this.pos = position;
        this.velocity = velocityVector;

        this.timestamp = t;
    }
}

/// <summary>
/// Компонент сетевой репликациии состояния игрока.
/// Посылает команды от владельца на сервер, рассылает всем остальным.
/// </summary>
[RequireComponent(typeof(GetOverInputControl))]
public class GetOverPlayer : NetworkPlayerReplicator {

    GetOverInputControl inputControl;

    List<StateSnapshot> stateHistory = new List<StateSnapshot>();

    Coroutine recordHistory;

    Rigidbody rb;

    NetworkIdentity networkIdentity;

    [SyncVar]
    Vector3 startPosition;

    [SyncVar]
    Quaternion startRotation;

    Vector3 correctedPosition;

    int networkTimestampDelayMS;

    public bool trustOwner = false;

    [SyncVar]
    bool active;

    [SyncVar]
    string playerName;
    
    public void Freeze(bool b)
    {
        if (!b)
        {
            Debug.Log(name + " - <color=green>UnFreeze</color>");
        }
        else
            Debug.Log(name + " - <color=red>Freeze</color>");

        active = !b;
        if (isServer)
        {
            RpcFreeze(b);
        }
    }

    [ClientRpc]
    void RpcFreeze(bool b)
    {
        if (!isServer)
        {
            //Debug.Log("RpcFreeze "+b);
            Freeze(b);
        }
    }

    void Restart()
    {
        Debug.Log(name+ " - Restart()");

        transform.position = startPosition;

        transform.rotation = startRotation;

        inputControl.StateResync();

        if (isServer)
        {
            RpcRestart();
        }
    }

    [ClientRpc]
    void RpcRestart()
    {
        if (!isServer)
        {
            Restart();
        }
    }

    Text nameText;

    override protected void Awake()
    {
        base.Awake();

        nameText = transform.Find("Name").GetComponentInChildren<Text>();

        inputControl = GetComponent<GetOverInputControl>();

        rb = GetComponent<Rigidbody>();

        networkIdentity = GetComponent<NetworkIdentity>();
        
        recordHistory = StartCoroutine(RecordHistory(new WaitForSeconds(1/10f)));

        startPosition = transform.position;

        startRotation = transform.rotation;
    }

    IEnumerator RecordHistory( WaitForSeconds timer )
    {
        while (true)
        {
            AddHistory(new StateSnapshot(inputControl.clickPos, transform.position, rb.velocity, Time.realtimeSinceStartup));
            yield return timer;
        }
    }

    /// <summary>
    /// Вызывается только на владельце персонажа каждый кадр
    /// </summary>
    protected override void UpdateLocalPlayer()
    {
        if (!active)
            return;

        //base.UpdateLocalPlayer();
        inputSource.UpdateControls();

        inputControl.EvaluateControls();

        Debug.DrawLine(transform.position, closestPositionInHistory, Color.yellow);
    }

    /*protected override void UpdateRemotePlayer()
    {
        base.UpdateRemotePlayer();
    }*/

    private void FixedUpdate()
    {
        if (!active)
            return;

        if (isClient && !isServer && !(isLocalPlayer && trustOwner))
        {
            Vector3 newVelocity = (latestServerPos - rb.position) * 0.5f / GetNetworkSendInterval();

            rb.velocity = newVelocity;

            correctedPosition += (newVelocity * Time.fixedDeltaTime * 0.1f);         
        }
    }

    /// <summary>
    /// Вызывается только на владельце персонажа каждый сетевой фрейм
    /// </summary>
    override public void OnLocalPlayerNetUpdate()
    {
        if (!active)
            return;

        if (!isServer)
            Cmd_SetServerState(inputControl.clickPos, transform.position, rb.velocity, NetworkTransport.GetNetworkTimestamp());
        else
            Rpc_SetClientState(inputControl.clickPos, transform.position, rb.velocity, NetworkTransport.GetNetworkTimestamp()); // - networkTimestampDelayMS);
    }

    protected override void UpdateServer()
    {
        base.UpdateServer();

        //Rpc_SetClientState(inputControl.clickPos, transform.position, rb.velocity, NetworkTransport.GetNetworkTimestamp());
    }

    /// <summary>
    /// Получение управляющих команд на сервере.
    /// TODO: можно отправлять только позицию клика для экономии траффика, но сейчас есть возможность доверия клиенту при trustOwner = true.
    /// </summary>
    [Command]
    private void Cmd_SetServerState(Vector3 click, Vector3 pos, Vector3 vel, int timestamp)
    {
        networkTimestampDelayMS = GetMessageDelayMS(timestamp);

        if (trustOwner)
        {
            rb.MovePosition(pos);
            rb.velocity = vel;
        }

        inputControl.clickPos = click;

        Rpc_SetClientState(click, transform.position, rb.velocity, NetworkTransport.GetNetworkTimestamp()); // - networkTimestampDelayMS);
    }


    float positionError;
    Vector3 positionErrorVector;
    
    Vector3 closestPositionInHistory;
    Vector3 latestServerPos;

    float latestCorrectionTime;

    public float toleratedError = 0.3f;
    
    /// <summary>
    /// Установка параметров на клиенте.
    /// </summary>
    [ClientRpc]
    private void Rpc_SetClientState(Vector3 click, Vector3 pos, Vector3 vel, int timestamp)
    {
        if (isServer)
            return;

        networkTimestampDelayMS = GetMessageDelayMS(timestamp);

        if (!IsLocalPlayer)
        {
            inputControl.clickPos = click;

        }
        else if (trustOwner)
        {
            return;
        }
        //inputControl.EvaluateControls();

        Debug.DrawLine(latestServerPos, pos, Color.blue, 1.0f);

        latestServerPos = pos;

        float historyTime = Time.realtimeSinceStartup;// - networkTimestampDelayMS * 0.001f;

        /// TODO: проверку позиции в истории можно убрать. Простая синхронизация скорости достаточна.
        //closestPositionInHistory = CheckPositionInHistory(new StateSnapshot(click, pos, vel, historyTime), out positionErrorVector);
        positionErrorVector = pos - transform.position;

        positionError = positionErrorVector.magnitude;

        /// при большом расхождении с записью в истории, производим полную ре-синхронизацию
        if (positionError > toleratedError && historyTime > latestCorrectionTime)
        {
            //Debug.DrawLine(transform.position, pos, Color.red, 3f);

            latestCorrectionTime = Time.realtimeSinceStartup;

            correctedPosition = pos;

            rb.Sleep();

            transform.position = pos;

            rb.velocity = vel;

            inputControl.StateResync();
        }   
    }

    int GetMessageDelayMS(int timestamp)
    {
        int networkTimestampDelayMS = 0;

        if (isServer)
        {
            byte error;
            networkTimestampDelayMS = NetworkTransport.GetRemoteDelayTimeMS(
                networkIdentity.connectionToClient.hostId,
                networkIdentity.connectionToClient.connectionId,
                timestamp,
                out error);

            //Debug.Log("COMMAND delay = "+ networkTimestampDelayMS);
        }
        else
        {
            byte error;
            networkTimestampDelayMS = NetworkTransport.GetRemoteDelayTimeMS(
                NetworkManager.singleton.client.connection.hostId,
                NetworkManager.singleton.client.connection.connectionId,
                timestamp,
                out error);
        }
        
        return networkTimestampDelayMS;
    }

    Vector3 CheckPositionInHistory(StateSnapshot msg, out Vector3 diff)
    {
        Vector3 closestPos = msg.pos;

        float distance = -1f;

        diff = Vector3.zero;
        
        float messageTime = msg.timestamp;

        int historyCount = stateHistory.Count;

        bool found = false;
        //check if oldest record time is less than messageTime
        if (historyCount > 0) // && positionHistory[0].timestamp < messageTime
        {

            // Go through buffer and find correct state to play back
            for (int i = 0; i < historyCount; i++)
            {
                //int prevI = Mathf.Max(0, i - 1);

                // The state one slot newer (<100ms) than the best playback state
                StateSnapshot newerState = stateHistory[i];

                // The best playback state (closest to 100 ms old (default time))
                StateSnapshot olderState = stateHistory[Mathf.Max(0, i - 1)];

                float segmentTime = (newerState.timestamp - olderState.timestamp);

                float msgTime = (msg.timestamp - olderState.timestamp);

                float progress = msgTime / segmentTime;

                Vector3 olderStatePos = olderState.pos + diff;

                Vector3 newerStatePos = newerState.pos + diff;

                /*
                if ((Time.realtimeSinceStartup - latestCorrectionTime) <= GetNetworkSendInterval())
                {
                    Debug.DrawLine((olderStatePos), (newerStatePos), Color.yellow, GetNetworkSendInterval());
                }
                else
                {
                    if (!found)
                        Debug.DrawLine((olderStatePos), (newerStatePos), Color.black, GetNetworkSendInterval());
                    else
                        Debug.DrawLine((olderStatePos), (newerStatePos), Color.red, GetNetworkSendInterval()); 
                }*/

                if (newerState.timestamp < messageTime)
                {
                    Debug.DrawLine((olderStatePos), (newerStatePos), Color.grey, GetNetworkSendInterval());
                }
                else
                {
                    if (!found)
                    {
                        found = true;

                        closestPos = olderStatePos + (newerStatePos - olderStatePos) * progress;// + Vector3.Project(msg.pos - olderState.pos, newerState.pos - olderState.pos);

                        diff = (msg.pos - closestPos);

                        distance = diff.magnitude;

                        Debug.DrawLine((msg.pos), (closestPos), Color.magenta, 1f);
                    }

                    Debug.DrawLine((olderStatePos), (newerStatePos), Color.black, GetNetworkSendInterval());
                    
                    //offset += diff;
                    //break;
                }
            }


            //error = (msg.pos-closestPos).magnitude;

            //Debug.DrawLine((msg.pos), (closestPos), Color.green, 0.05f);
            //Debug.DrawLine(transform.position, transform.parent.TransformPoint(msg.pos), Color.cyan, 0.05f);

        }

        return closestPos;
    }

    void AddHistory(StateSnapshot snapshot, int limit = 100)
    {
        stateHistory.Add(snapshot);

        int historyCount = stateHistory.Count;

        if (stateHistory.Count > limit) { stateHistory.RemoveRange(0, historyCount - (limit-1)); stateHistory.TrimExcess(); }
    }

    float averageDelay;

    override protected void Update()
    {
        base.Update();

        averageDelay = Mathf.Lerp(averageDelay, networkTimestampDelayMS, averageDelay == 0f? 1f : Time.deltaTime*0.5f);

        if (isServer)
            playerName = name;
        else {
            if (name != playerName)
                name = playerName;
        }

        nameText.text = name;
    }

    public bool showStats = true;

    private void OnGUI()
    {
        if (!showStats || Camera.main == null)
            return;
        
        Rect infoRect = new Rect(Screen.width / 2 - 50, 25, 100, 50);

       

        if (!isLocalPlayer)
        {
            Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position + Vector3.up);
            infoRect = new Rect(screenPos.x - 50, Screen.height - (screenPos.y + 50), 100, 50);
        }
        

        NetworkConnection c = isServer ? connectionToClient : connectionToServer;
        if (c != null && (isServer || isLocalPlayer) && c.hostId >= 0)
        {
            float delay = averageDelay;//NetworkTransport.GetCurrentRtt(c.hostId, c.connectionId, out error);
            Color textColor = active ? Color.Lerp(Color.green, Color.red, delay / 100f) : Color.grey;

            var centeredStyle = GUI.skin.GetStyle("Label");
            centeredStyle.alignment = TextAnchor.UpperCenter;
            centeredStyle.fontStyle = FontStyle.Bold;

            //GUI.Label(infoRect, delay.ToString("F1") + " ms", centeredStyle);
            OnGUIEffects.DrawOutline(infoRect, delay.ToString("F1") + " ms", centeredStyle, Color.black, textColor, 2);

            //GUI.Button(infoRect, delay.ToString("F1")+" ms");
        }
    }
}
