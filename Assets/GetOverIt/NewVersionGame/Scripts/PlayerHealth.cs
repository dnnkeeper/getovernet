﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;


public class PlayerHealth : NetworkBehaviour {

    [SyncVar]
    public float health;

    public UnityFloatEvent onDamaged;

    float _health;

    public UnityFloatEvent onHealthChanged;

    public UnityEvent onDied;

    public float rounding = 1f;

    private void Restart()
    {
        health = 100f;
    }
    
    public void ApplyDamage(float amount)
    {
        amount = amount - amount % rounding;

        if (health > amount)
        {
            //Debug.Log("ApplyDamage "+amount);
            health -= amount;
            onDamaged.Invoke(amount);
        }
        else
        {
            health = 0;
            onDied.Invoke();
        }
    }

    private void Update()
    {
        if (_health != health)
        {
            _health = health;
            onHealthChanged.Invoke(health);
        }
    }

    public void OnDamage(DamageInfo damageInfo)
    {
        if (isServer)
        {
            if (!isLocalPlayer)
                ApplyDamage(damageInfo.amount);
            RpcApplyDamage(damageInfo.amount);
        }
    }

    [ClientRpc]
    public void RpcApplyDamage(float amount)
    {
        ApplyDamage(amount);
    }

    private void OnTriggerEnter(Collider other)
    {
        
        DamageCollider damager = other.GetComponent<DamageCollider>();

        if (damager != null)
        {
            Debug.Log("OnTriggerEnter " + other);
            OnDamage(new DamageInfo(damager.damageAmount, damager.damageType, damager.gameObject) );
        }
    }

}
