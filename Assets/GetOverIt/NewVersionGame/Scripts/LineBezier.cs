﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineBezier : MonoBehaviour
{
	public LineRenderer lineRenderer;
	public Transform point0, point1, point2, point3;

	public int numPoints = 25; // 4,7,13,25,49....
	private Vector3[] positions = new Vector3[25];

	void Start () 
	{
		positions = new Vector3[numPoints];
		//lineRenderer.SetVertexCount(numPoints);
		lineRenderer.positionCount = numPoints;
		//DrawLinearCurve();
		//DrawQuadraticCurve();
		DrawCubicCurve();

	}
	
	void Update () 
	{
		DrawCubicCurve();

	}

	private void DrawLinearCurve() 
	{
		for(int i = 1; i < numPoints + 1; i++){
			float t = i / (float)numPoints;
			positions[i - 1] = CalculateLinearBezierPoint(t, point0.position, point1.position);
		}

		for(int i = 0; i < positions.Length; i++) {
			lineRenderer.SetPosition(i, positions[i]);
		}
	}

	private void DrawQuadraticCurve()
	{
		for(int i = 1; i < numPoints + 1; i++) {
			float t = i / (float)numPoints;
			positions[i - 1] = CalculateQuadraticBezierPoint(t, point0.position, point1.position, point2.position);
		}

		for(int i = 0; i < positions.Length; i++) {
			lineRenderer.SetPosition(i, positions[i]);
		}
	}

	private void DrawCubicCurve()
	{
		for(int i = 1; i < numPoints + 1; i++) {
			float t = i / (float)numPoints;
			positions[i - 1] = CalculateCubicBezierPoint(t, point0.position, point1.position, point2.position, point3.position);
		}

		for(int i = 0; i < positions.Length; i++) {
			lineRenderer.SetPosition(i, positions[i]);
		}
	}

	private Vector3 CalculateLinearBezierPoint(float t, Vector3 p0, Vector3 p1) {
		return p0 + t * (p1 - p0);
	}

	private Vector3 CalculateQuadraticBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2) {
		float u = 1 - t;
		float tt = t * t;
		float uu = u * u;
		Vector3 p = uu * p0;
		p += 2 * u * t * p1;
		p += tt * p2;
		return p;
	}

	private Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3) {
		float u = 1 - t;
		float tt = t * t;
		float uu = u * u;
		float uuu = uu * u;
		float ttt = tt * t;
		Vector3 p = uuu * p0;
		p += 3 * uu * t * p1;
		p += 3 * u * tt * p2;
		p += ttt * p3;
		return p;
	}
}
