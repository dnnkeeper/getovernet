﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct BoneProperties
{
	public string nameBone;
	public string IDLink;

	public BoneProperties(string name, string id)
	{
		this.nameBone = name;
		this.IDLink = id;
	}
}
