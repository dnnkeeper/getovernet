﻿using UnityEngine;

public class HideObject : MonoBehaviour
{
    [Range(1, 10)]
    [Header("Количество скрываемых объектов")]
    public static int objectCount;

    public GameObject[] hideObject = new GameObject[objectCount];

    void OnTriggerEnter(Collider a)
    {
        MeshRendererEnabler(a, false);
    }

    void OnTriggerExit(Collider a)
    {
        MeshRendererEnabler(a, true);
    }

    void MeshRendererEnabler(Collider coll, bool enabled)
    {
        if (coll.tag == "Player")
        {
            foreach (var i in hideObject)
            {
                i.GetComponent<MeshRenderer>().enabled = enabled;
            }
        }
    }
}
