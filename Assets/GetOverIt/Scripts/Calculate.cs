﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calculate : MonoBehaviour {

	public SkinnedMeshRenderer rend;

	public int numBones;
	public string[] nameBones;

	public bool saveBone = false;

	// Use this for initialization
	void Start () {
		numBones = rend.bones.Length;

		nameBones = new string[numBones];
		Transform[] saveBones = new Transform[numBones];
		Transform[] newBones = new Transform[numBones];

		for(int i = 0; i<numBones; i++) {
			nameBones[i] = rend.bones[i].name;
			saveBones[i] = rend.bones[i];
		}

		if(saveBone) {
			newBones[0] = saveBones[1];
			newBones[1] = saveBones[0];

			rend.bones = newBones;
			Mesh mesh = rend.sharedMesh;
			mesh.RecalculateNormals();
		}

		for(int i = 0; i<numBones; i++) {
			nameBones[i] = rend.bones[i].name;

		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
