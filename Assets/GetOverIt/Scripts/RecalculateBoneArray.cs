﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class RecalculateBoneArray : MonoBehaviour
{
	public BoneProperties[] readBoneValue;

	[Header ("Список - в какой последовательности должны идти кости")]
	private string[] trueArray = new string[49]
	{"Root",
	 "Hips",
	"Spine_01",
	"Spine_02",
	"Spine_03",
	"Neck",
	"Head",
	"Eyes",
	"Eyebrows",
	"Clavicle_L",
	"Shoulder_L",
	"Elbow_L",
	"Hand_L",
	"Thumb_01_L",
	"Thumb_02_L",
	"Thumb_03_L",
	"IndexFinger_01_L",
	"IndexFinger_02_L",
	"IndexFinger_03_L",
	"IndexFinger_04_L",
	"Finger_01_L",
	"Finger_02_L",
	"Finger_03_L",
	"Finger_04_L",
	"Clavicle_R",
	"Shoulder_R",
	"Elbow_R",
	"Hand_R",
	"Thumb_01_R",
	"Thumb_02_R",
	"Thumb_03_R",
	"IndexFinger_01_R",
	"IndexFinger_02_R",
	"IndexFinger_03_R",
	"IndexFinger_04_R",
	"Finger_01_R",
	"Finger_02_R",
	"Finger_03_R",
	"Finger_04_R",
	"UpperLeg_R",
	"LowerLeg_R",
	"Ankle_R",
	"Ball_R",
	"Toes_R",
	"UpperLeg_L",
	"LowerLeg_L",
	"Ankle_L",
	"Ball_L",
	"Toes_L"};


	[Header ("Данные о порядке костей - read")]
	[TextArea(3,10)]
	public string input;
		
	[Header ("Данные о порядке костей - return")]
	public string IDmeshes;
	[TextArea(3,10)]
	public string output;



	// Debug
	[TextArea(3,10)]
	public string[] spl;

	void Start () 
	{
		// разбиваем данные по группам. Группы в исходных данных отделены ";"
		spl = input.Split(new char [] {';'});

		readBoneValue = new BoneProperties[spl.Length];

		for(int i = 0; i < spl.Length; i++) {
			SplitBase(spl[i], i);
		}

		for(int i = 0; i < spl.Length; i++) {
			for(int y = 0; y < spl.Length; y++) {
				if(trueArray[i] == readBoneValue[y].nameBone) {
					print(readBoneValue[y].nameBone);
					CreateBase(y);
				}
			}
		}
	}
	
	void SplitBase(string str, int IDarray)
	{
		// Определяем имя кости
		string getNameBone = str.Substring(7);
		char ch = ',';
		int indexCh = getNameBone.IndexOf(ch);
		getNameBone = getNameBone.Substring(0, indexCh);

		// Определяем ее ID
		string subString = "OO";
		int indexSt = str.IndexOf(subString);
		string getIDBone = str.Substring(indexSt + 4);
		subString = ",";
		indexSt = getIDBone.IndexOf(subString);
		getIDBone = getIDBone.Substring(indexSt + 1);
		getIDBone = getIDBone.Substring(0, 10);

		readBoneValue[IDarray] = new BoneProperties(getNameBone, getIDBone);
	}

	void CreateBase(int IDarray)
	{
		string basic =  ";SubDeformer::, Deformer::\n" +
						"C: " + '"' + "OO" + '"' + "," + readBoneValue[IDarray].IDLink + "," + IDmeshes;

		basic += "\n\n";
		output += basic;
	}
}
