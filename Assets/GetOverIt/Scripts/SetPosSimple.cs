﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPosSimple : MonoBehaviour {

    public Camera CameraRef;

    public void SetPos(Vector3 v)
    {
        /*if (CameraRef != null)
        {
            Plane objectPlane = new Plane(CameraRef.transform.forward, transform.position);
            Ray clickRay = CameraRef.ScreenPointToRay(v);
            float distance = 0f;
            if (objectPlane.Raycast(clickRay, out distance))
            {
                v = clickRay.origin + clickRay.direction * distance;
            }
        }*/
        transform.position = v;
    }
}
