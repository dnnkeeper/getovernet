﻿using UnityEngine;

public class Simple2DMove : MonoBehaviour {

    public float speed = 0.1f;

	void Update () {
        transform.Translate(transform.right * Input.GetAxis("Horizontal") * speed);
        transform.Translate(transform.up * Input.GetAxis("Vertical") * speed);
    }
}
