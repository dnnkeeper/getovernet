﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharIK : MonoBehaviour
{
    public Transform origin, target;

    [Header("Left")]
    public Vector3 leftRot;
    public Vector3 leftPos;

    [Header("Left-Down")]
    public Vector3 leftDownRot;
    public Vector3 leftDownPos;

    [Header("Down")]
    public Vector3 downRot;
    public Vector3 downPos;

    [Header("Down-Right")]
    public Vector3 downRightRot;
    public Vector3 downRightPos;

    [Header("Right")]
    public Vector3 rightRot;
    public Vector3 rightPos;

    [Header("Right-Up")]
    public Vector3 rightUpRot;
    public Vector3 rightUpPos;

    [Header("Up")]
    public Vector3 upRot;
    public Vector3 upPos;

    [Header("Up-Left")]
    public Vector3 upLeftRot;
    public Vector3 upLeftPos;

    private Quaternion leftR, rightR, downR, upR;
    private Quaternion leftDownR, downRightR, rightUpR, upLeftR;

    private Vector3[] positions;
    private Quaternion[] rotations;

    [Header("Debug")]
    public bool debug;
    public int directionNum;
    public int lerpAngle;

    private void Start()
    {
        leftR = Quaternion.Euler(leftRot);
        rightR = Quaternion.Euler(rightRot);
        downR = Quaternion.Euler(downRot);
        upR = Quaternion.Euler(upRot);

        leftDownR = Quaternion.Euler(leftDownRot);
        downRightR = Quaternion.Euler(downRightRot);
        rightUpR = Quaternion.Euler(rightUpRot);
        upLeftR = Quaternion.Euler(upLeftRot);

        positions = new[] { leftPos, leftDownPos, downPos, downRightPos, rightPos, rightUpPos, upPos, upLeftPos };
        rotations = new[] { leftR, leftDownR, downR, downRightR, rightR, rightUpR, upR, upLeftR };
    }

    private void Update()
    {
        var dir = target.position - origin.position;
        var proj = Vector3.ProjectOnPlane(dir, Vector3.forward);
        var angle = Mathf.Rad2Deg * Mathf.Atan2(proj.y, proj.x) + 180;

        // 0 - left
        // 90 - down
        // 180 - right
        // 270 - up

        if (debug)
        {
            leftR = Quaternion.Euler(leftRot);
            rightR = Quaternion.Euler(rightRot);
            downR = Quaternion.Euler(downRot);
            upR = Quaternion.Euler(upRot);
            leftDownR = Quaternion.Euler(leftDownRot);
            downRightR = Quaternion.Euler(downRightRot);
            rightUpR = Quaternion.Euler(rightUpRot);
            upLeftR = Quaternion.Euler(upLeftRot);

            positions[0] = leftPos;
            positions[1] = leftDownPos;
            positions[2] = downPos;
            positions[3] = downRightPos;
            positions[4] = rightPos;
            positions[5] = rightUpPos;
            positions[6] = upPos;
            positions[7] = upLeftPos;

            rotations[0] = leftR;
            rotations[1] = leftDownR;
            rotations[2] = downR;
            rotations[3] = downRightR;
            rotations[4] = rightR;
            rotations[5] = rightUpR;
            rotations[6] = upR;
            rotations[7] = upLeftR;
        }

        float step = 360 / positions.Length;
        for (int i = 1; i < positions.Length; i++)
        {
            if (angle < i * step)
            {
                if (debug)
                {
                    directionNum = i;
                    lerpAngle = (int)((angle - (i - 1) * step));
                }

                transform.localPosition = Vector3.Lerp(positions[i - 1], positions[i], (angle - (i - 1) * step) / step);
                transform.rotation = Quaternion.Slerp(rotations[i - 1], rotations[i], (angle - (i - 1) * step) / step);
                return;
            }
        }

        if (debug)
        {
            directionNum = 8;
            lerpAngle = (int)((angle - 360 + step));
        }

        transform.localPosition = Vector3.Lerp(positions[positions.Length - 1], positions[0], (angle - 360 + step) / step);
        transform.rotation = Quaternion.Slerp(rotations[rotations.Length - 1], rotations[0], (angle - 360 + step) / step);
    }
}