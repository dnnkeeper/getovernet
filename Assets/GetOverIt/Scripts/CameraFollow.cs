﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float speed;
    public Vector2 offset;
    public Mode mode = Mode.Legacy;

    public enum Mode
    {
        Legacy,
        MoveTowards,
    }

    void Update()
    {
        switch (mode)
        {
            case Mode.Legacy:
                Vector3 currentPos = this.transform.position;
                Vector3 endPosition = target.position;

                if (currentPos.x < endPosition.x)
                {
                    currentPos.x += Time.deltaTime * speed;
                    if (currentPos.x > endPosition.x)
                        currentPos.x = endPosition.x;
                }
                else
                {
                    currentPos.x -= Time.deltaTime * speed;
                    if (currentPos.x < endPosition.x)
                        currentPos.x = endPosition.x;
                }

                if (currentPos.y < endPosition.y)
                {
                    currentPos.y += Time.deltaTime * speed;
                    if (currentPos.y > endPosition.y)
                        currentPos.y = endPosition.y;
                }
                else
                {
                    currentPos.y -= Time.deltaTime * speed;
                    if (currentPos.y < endPosition.y)
                        currentPos.y = endPosition.y;
                }

                this.transform.position = new Vector3(currentPos.x, currentPos.y, currentPos.z);
                break;

            case Mode.MoveTowards:
                var targetPos = target.position;
                Vector3 current = transform.position;
                current.x = Mathf.MoveTowards(current.x, targetPos.x + offset.x, Time.deltaTime * speed);
                current.y = Mathf.MoveTowards(current.y, targetPos.y + offset.y, Time.deltaTime * speed);
                transform.position = current;
                break;
        }
    }
}